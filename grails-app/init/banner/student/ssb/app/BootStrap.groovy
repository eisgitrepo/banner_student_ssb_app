/*******************************************************************************
 Copyright 2009-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package banner.student.ssb.app

import groovy.util.logging.Slf4j
import net.hedtech.banner.converters.json.JSONBeanMarshaller
import net.hedtech.banner.converters.json.JSONDomainMarshaller
import net.hedtech.banner.converters.json.StudentDomainMarshaller
import net.hedtech.banner.general.person.PersonAddress
import net.hedtech.banner.general.system.State
import net.hedtech.banner.i18n.JavaScriptMessagesTagLib
import net.hedtech.banner.i18n.LocalizeUtil
import grails.converters.JSON
import net.hedtech.banner.student.ClasslistDecorator
import net.hedtech.banner.student.faculty.FacultyAdvisorContactCard
import net.hedtech.banner.student.faculty.FacultyClasslistSummaryView
import net.hedtech.banner.student.faculty.FacultyDropRosterSummaryView
import net.hedtech.banner.student.faculty.FacultyMemberDepartmentAndCollegeInformation

import net.hedtech.banner.student.overall.ConcurrentCurriculaDecorator
import net.hedtech.banner.student.CourseListDecorator
import net.hedtech.banner.student.DropRosterActionsDecorator
import net.hedtech.banner.student.DropRosterMaintenanceDecorator
import net.hedtech.banner.student.DropRosterStatusSectionConflictDecorator
import net.hedtech.banner.student.DropRosterStatusSectionDisableDecorator
import net.hedtech.banner.student.DropRosterStatusSectionSummaryDecorator
import net.hedtech.banner.student.WaitlistDecorator
import net.hedtech.banner.student.profile.StudentContactCard
import org.apache.commons.logging.LogFactory

import grails.core.ApplicationAttributes
import org.grails.plugins.web.taglib.ValidationTagLib
import org.grails.web.converters.marshaller.ClosureObjectMarshaller
import org.grails.web.converters.marshaller.json.GroovyBeanMarshaller
import net.hedtech.banner.student.profile.StudentNoteReadOnlyDecorator
import net.hedtech.banner.student.profile.StudentReleasableHoldDecorator
import net.hedtech.banner.marshalling.CustomMarshallerUtility
import grails.util.Holders as CH
import org.grails.web.converters.marshaller.json.ValidationErrorsMarshaller

/**
 * Executes arbitrary code at bootstrap time.
 * Code executed includes:
 * -- Configuring the dataSource to ensure connections are tested prior to use
 * */

@Slf4j
class BootStrap {



    def localizer = { mapToLocalize ->
        new ValidationTagLib().message(mapToLocalize)
    }

    def grailsApplication
    def resourceService
    def dateConverterService
    def javaScriptMessagesTagLib = new JavaScriptMessagesTagLib()
    def init = { servletContext ->
        javaScriptMessagesTagLib.getJsFilesList(grailsApplication)
        def ctx = servletContext.getAttribute(ApplicationAttributes.APPLICATION_CONTEXT)
        servletContext.finaidUiPluginPath = CH.pluginManager.getGrailsPlugin("bannerFinaidUi").getPluginPath();

        grailsApplication.controllerClasses.each {
            log.info "adding log property to controller: $it"
            // Note: weblogic throws an error if we try to inject the method if it is already present
            if (!it.metaClass.methods.find { m -> m.name.matches("getLog") }) {
                def name = it.name // needed as this 'it' is not visible within the below closure...
                try {
                    it.metaClass.getLog = { LogFactory.getLog("$name") }
                }
                catch (e) { } // rare case where we'll bury it...
            }
        }

        grailsApplication.allClasses.each {
            if (it.name?.contains("plugin.resource")) {
                log.info "adding log property to plugin.resource: $it"

                // Note: weblogic throws an error if we try to inject the method if it is already present
                if (!it.metaClass.methods.find { m -> m.name.matches("getLog") }) {
                    def name = it.name // needed as this 'it' is not visible within the below closure...
                    try {
                        it.metaClass.getLog = { LogFactory.getLog("$name") }
                    }
                    catch (e) { } // rare case where we'll bury it...
                }
            }
        }

        // Register the JSON Marshallers for format conversion and XSS protection
        registerJSONMarshallers()

        //resourceService.reloadAll()
    }
    def destroy = {
    // no-op
    }
    private def registerJSONMarshallers() {
        JSON.registerObjectMarshaller(Date) {
            dateConverterService.parseGregorianToDefaultCalendar(LocalizeUtil.formatDate(it))
        }

        def localizeMap = [
                'attendanceHour': LocalizeUtil.formatNumber,
                'transferHours': LocalizeUtil.formatNumber
        ]
        JSON.registerObjectMarshaller(new JSONBeanMarshaller( localizeMap ), 2) // for decorators and maps
        JSON.registerObjectMarshaller(new JSONDomainMarshaller( localizeMap, true), 3) // for domain objects
        JSON.registerObjectMarshaller(new ValidationErrorsMarshaller(),4)

        JSON.createNamedConfig("StudentNoteRO") {cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(StudentNoteReadOnlyDecorator, {o, c ->
                new StudentDomainMarshaller().marshalObject(o, c)
            }))
        }

        JSON.createNamedConfig("StudentHold") {cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(StudentReleasableHoldDecorator, {o, c ->
                new StudentDomainMarshaller().marshalObject(o, c)
            }))
        }

        JSON.createNamedConfig("FacultyContactCardRender") {cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(FacultyAdvisorContactCard, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(FacultyMemberDepartmentAndCollegeInformation, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(PersonAddress, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(State, {state ->
                return [
                        code: state.code,
                        description: state.description
                ]
            }))
        }
        JSON.createNamedConfig("ClassListCustomMarshaller") {cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(CourseListDecorator, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(ClasslistDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(FacultyClasslistSummaryView, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(WaitlistDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(ConcurrentCurriculaDecorator, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
        }
       // CustomMarshallerUtility.registerExcludeMarshallerForDecorator("StudentNoteRO", StudentNoteReadOnlyDecorator, ['pidm', 'student'])
       // CustomMarshallerUtility.registerExcludeMarshallerForDecorator("StudentHold", StudentReleasableHoldDecorator, ['pidm', 'sourcePidm', 'approvePidm', 'termPidm', 'student', 'holdType', 'originator'])


        JSON.createNamedConfig("ContactCardRender") {cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(StudentContactCard, { student ->
                return [
                        bannerId: student.bannerId,
                        lastName: student.lastName.encodeAsHTML(),
                        firstName: student.firstName.encodeAsHTML(),
                        middleName: student.middleName.encodeAsHTML(),
                        surnamePrefix: student.surnamePrefix.encodeAsHTML(),
                        displayName: student.displayName,
                        isConfidentialStudent: student.isConfidentialStudent,
                        primaryProgram: student.primaryProgram,
                        primaryMajor: student.primaryMajor,
                        emailAddress: student.emailAddress,
                        telephoneNumber: student.telephoneNumber,
                        address: student.address
                ]
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(PersonAddress, {o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(State, {state ->
                return [
                        code: state.code,
                        description: state.description
                ]
            }))
        }

        JSON.createNamedConfig("DropRosterStatusCustomMarshaller") { cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(DropRosterStatusSectionSummaryDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))

            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(DropRosterStatusSectionDisableDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))

            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(DropRosterStatusSectionConflictDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(FacultyDropRosterSummaryView, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
        }

        JSON.createNamedConfig("DropRosterMaintenanceCustomMarshaller") { cfg ->
            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(DropRosterMaintenanceDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))

            cfg.registerObjectMarshaller(new ClosureObjectMarshaller(DropRosterActionsDecorator, { o, c ->
                new GroovyBeanMarshaller().marshalObject(o, c)
            }))
        }
    }
}
