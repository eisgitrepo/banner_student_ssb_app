/*******************************************************************************
 Copyright 2013-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/

//import net.hedtech.banner.configuration.ApplicationConfigurationUtils as ConfigFinder
//import grails.plugin.springsecurity.SecurityConfigType

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

//grails.config.locations = [] // leave this initialized to an empty list, and add your locations in the map below.
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '*.png', '/plugins/*']


grails.config.locations = [
        BANNER_APP_CONFIG        : "banner_configuration.groovy",
        BANNER_STUDENT_SSB_CONFIG: "StudentSelfService_configuration.groovy",
        BANNER_STUDENT_UI_CONFIG : "bannerStudentAdvisorUI_configuration.properties"
]

grails.databinding.useSpringBinder = true
/// ******************************************************************************
//
//                       +++ BUILD NUMBER SEQUENCE UUID +++
//
// ******************************************************************************
//
// A UUID corresponding to this project, which is used by the build number generator.
// Since the build number generator web service provides build number sequences to
// multiple projects, and each project uses a unique UUID to identify which number
// sequence it is using.
//
// This number should NOT be changed.
// FYI: When a new UUID is needed (e.g., for a new project), use this URI:
//      http://maldevl2.sungardhe.com:8080/BuildNumberServer/newUUID
//
// DO NOT EDIT THIS UUID UNLESS YOU ARE AUTHORIZED TO DO SO AND KNOW WHAT YOU ARE DOING
//
build.number.uuid = "7f8235d8-2a51-4f2f-8516-47d913caf346" // specific UUID for Advisor solution
build.number.base.url = "http://m039198.ellucian.com:8080/BuildNumberServer/buildNumber?method=getNextBuildNumber&uuid="
app.name = "StudentSelfService"
app.platform.version = "9.32.3"
app.appId = "SSS"



server.'contextPath' = '/StudentSelfService'
server.port = 8090
spring.jmx.enabled = false


grails.project.groupId = "net.hedtech" // used when deploying to a maven repo
grails.databinding.useSpringBinder = true
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
        html         : ['text/html', 'application/xhtml+xml'],
        xml          : ['text/xml', 'application/xml', 'application/vnd.sungardhe.student.v0.01+xml'],
        text         : 'text/plain',
        js           : 'text/javascript',
        rss          : 'application/rss+xml',
        atom         : 'application/atom+xml',
        css          : 'text/css',
        csv          : 'text/csv',
        all          : '*/*',
        json         : ['application/json', 'text/json'],
        form         : 'application/x-www-form-urlencoded',
        multipartForm: 'multipart/form-data',
        jpg          : 'image/jpeg',
        png          : 'image/png',
        gif          : 'image/gif',
        bmp          : 'image/bmp',
        svg          : 'image/svg+xml',
        svgz         : 'image/svg+xml'
]

// The default codec used to encode data with ${}
grails.views.default.codec = "html" // none, html, base64  **** note: Setting this to html will ensure html is escaped, to prevent XSS attack ****
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
grails.plugin.springsecurity.logout.afterLogoutUrl = "/"
grails.plugin.springsecurity.logout.mepErrorLogoutUrl = '/logout/logoutPage'
grails.converters.domain.include.version = true
//grails.converters.json.date = "default"

grails.converters.json.pretty.print = true
grails.converters.json.default.deep = true

// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = false

// enable GSP preprocessing: replace head -> g:captureHead, title -> g:captureTitle, meta -> g:captureMeta, body -> g:captureBody
grails.views.gsp.sitemesh.preprocess = true
//grails.resources.rewrite.css = false
grails.resources.adhoc.excludes = ['/**/*-custom.css']


environments {
    development {
        grails.resources.debug = true
    }
}

// ******************************************************************************
//
//                       +++ DATA ORIGIN CONFIGURATION +++
//
// ******************************************************************************
// This field is a Banner standard, along with 'lastModifiedBy' and lastModified.
// These properties are populated automatically before an entity is inserted or updated
// within the database. The lastModifiedBy uses the username of the logged in user,
// the lastModified uses the current timestamp, and the dataOrigin uses the value
// specified here:
dataOrigin = "Banner"

// ******************************************************************************
//
//                       +++ FORM-CONTROLLER MAP +++
//
// ******************************************************************************
// This map relates controllers to the Banner forms that it replaces.  This map
// supports 1:1 and 1:M (where a controller supports the functionality of more than
// one Banner form.  This map is critical, as it is used by the security framework to
// set appropriate Banner security role(s) on a database connection. For example, if a
// logged in user navigates to the 'medicalInformation' controller, when a database
// connection is attained and the user has the necessary role, the role is enabled
// for that user and Banner object.

formControllerMap = [
        'banner'                            : ['SCACRSE'],
        'mainpage'                          : ['SELFSERVICE'],
        'menu'                              : ['SELFSERVICE'],
        'theme'                             : ['SELFSERVICE'],
        'themeeditor'                       : ['SELFSERVICE'],
        'studentprofile'                    : ['SELFSERVICE'],
        'studentattendancetracking'         : ['SELFSERVICE'],
        'studentcommondashboard'            : ['SELFSERVICE'],
        'landingpagepicture'                : ['SELFSERVICE'],
        'studentpassword'                   : ['SELFSERVICE'],
        'prioreducationandtesting'          : ['SELFSERVICE'],
        'studentpicture'                    : ['SELFSERVICE'],
        'facultycard'                       : ['SELFSERVICE'],
        'facultypicture'                    : ['SELFSERVICE'],
        'studentcard'                       : ['SELFSERVICE'],
        'studentholds'                      : ['SELFSERVICE'],
        'studentnotes'                      : ['SELFSERVICE'],
        'termselection'                     : ['SELFSERVICE'],
        'adviseesearch'                     : ['SELFSERVICE'],
        'selfservicemenu'                   : ['SELFSERVICE'],
        'adviseelist'                       : ['SELFSERVICE'],
        'adviseeemail'                      : ['SELFSERVICE'],
        'excelexport'                       : ['SELFSERVICE'],
        'listfilterbase'                    : ['SELFSERVICE'],
        'adviseelistfilter'                 : ['SELFSERVICE'],
        'adviseepicture'                    : ['SELFSERVICE'],
        'facultyadvisor'                    : ['ADVISINGAPI'],
        'advisors'                          : ['ADVISINGAPI'],
        'advisees'                          : ['ADVISINGAPI'],
        'adviseegrades'                     : ['ADVISINGAPI'],
        'advisee-assignment'                : ['ADVISINGAPI'],
        'advisee-search'                    : ['ADVISINGAPI'],
        'survey'                            : ['SELFSERVICE'],
        'useragreement'                     : ['SELFSERVICE'],
        'securityqa'                        : ['SELFSERVICE'],
        'selfservicemenu'                   : ['SELFSERVICE'],
        'studentgrades'                     : ['SELFSERVICE'],
        'studentgradespicture'              : ['SELFSERVICE'],
        'componentdetails'                  : ['SELFSERVICE'],
        'classlistapp'                      : ['SELFSERVICE'],
        'classlist'                         : ['SELFSERVICE'],
        'courselist'                        : ['SELFSERVICE'],
        'coursedetails'                     : ['SELFSERVICE'],
        'sectiondetails'                    : ['SELFSERVICE'],
        'classlistpicture'                  : ['SELFSERVICE'],
        'contactcard'                       : ['SELFSERVICE'],
        'waitlist'                          : ['SELFSERVICE'],
        'waitlistpicture'                   : ['SELFSERVICE'],
        'classlistexport'                   : ['SELFSERVICE'],
        'classliststudentcard'              : ['SELFSERVICE'],
        'studentdetails'                    : ['SELFSERVICE'],
        'droproster'                        : ['SELFSERVICE'],
        'droprosterstatus'                  : ['SELFSERVICE'],
        'droprostermaintenance'             : ['SELFSERVICE'],
        'droprosterpicture'                 : ['SELFSERVICE'],
        'droprosterexport'                  : ['SELFSERVICE'],
        'droprosterstudentcard'             : ['SELFSERVICE'],
        'uploadproperties'                  : ['SELFSERVICE'],
        'userpreference'                    : ['SELFSERVICE'],
        'terms'                             : ['GUAGMNU', 'SELFSERVICE'],
        'term-details'                      : ['GUAGMNU', 'SELFSERVICE'],
        'students'                          : ['GUAGMNU', 'SELFSERVICE'],
        'student-bioinfo'                   : ['GUAGMNU', 'SELFSERVICE'],
        'student-generalinfo'               : ['GUAGMNU', 'SELFSERVICE'],
        'student-contactcard'               : ['GUAGMNU', 'SELFSERVICE'],
        'student-curriculum'                : ['GUAGMNU', 'SELFSERVICE'],
        'student-academics'                 : ['GUAGMNU', 'SELFSERVICE'],
        'advisors'                          : ['GUAGMNU', 'SELFSERVICE'],
        'holds'                             : ['GUAGMNU', 'SELFSERVICE'],
        'student-prior-education'           : ['GUAGMNU', 'SELFSERVICE'],
        'testscores'                        : ['GUAGMNU', 'SELFSERVICE'],
        'registration-history'              : ['GUAGMNU', 'SELFSERVICE'],
        'about'                             : ['GUAGMNU'],
        'accountsummary'                    : ['SELFSERVICE'],
        'accountsummaryoverview'            : ['SELFSERVICE'],
        'accountsummarybyterm'              : ['SELFSERVICE'],
        'accountsummarybyperiod'            : ['SELFSERVICE'],
        'accountdetailbyterm'               : ['SELFSERVICE'],
        'accountdetailbytermfinaid'         : ['SELFSERVICE'],
        'accountdetailbytermmemo'           : ['SELFSERVICE'],
        'accountdetailbyterminstallmentplan': ['SELFSERVICE'],
        'studentaccountpdf'                 : ['SELFSERVICE'],
        'studentaccountpayment'             : ['SELFSERVICE'],
        'studenttaxnotification'            : ['SELFSERVICE'],
        'studenthold'                       : ['SELFSERVICE'],
        'accountinformation'                : ['SELFSERVICE'],
        'studentaccountdeposit'             : ['SELFSERVICE'],
        'statementandpaymenthistory'        : ['SELFSERVICE'],
        'binaryfiledownload'                : ['SELFSERVICE'],
        'studentaccountstatement'           : ['SELFSERVICE'],
        'depositprocessing'                 : ['SELFSERVICE'],
        'adviseeliststudentcard'            : ['SELFSERVICE'],

        // FinAid formControllerMap
        'applicantactiveaidyears'           : ['SELFSERVICE'],
        'awardhistoryaidyears'              : ['SELFSERVICE'],
        'awardhistory'                      : ['SELFSERVICE'],
        'awardletter'                       : ['SELFSERVICE'],
        'configurabletext'                  : ['SELFSERVICE'],
        'federalshoppingsheet'              : ['SELFSERVICE'],
        'financialaid'                      : ['SELFSERVICE'],
        'financialaiddashboard'             : ['SELFSERVICE'],
        'queansdialogbox'                   : ['SELFSERVICE'],
        'resources'                         : ['SELFSERVICE'],
        'studentacademicprogress'           : ['SELFSERVICE'],
        'studentnotifications'              : ['SELFSERVICE'],
        'webtextinfo'                       : ['SELFSERVICE'],
        'withdrawalinformation'             : ['SELFSERVICE'],

        'shortcut'                          : ['SELFSERVICE'],
        'userpreference'                    : ['SELFSERVICE'],
        'shortcut'				            : ['SELFSERVICE'],
        'login'                             : ['SELFSERVICE'],
        'logout'                            : ['SELFSERVICE']
]


grails.plugin.springsecurity.logout.afterLogoutUrl = "/"
//grails.plugin.springsecurity.useRequestMapDomainClass = false
//grails.plugin.springsecurity.rejectIfNoRule = true
grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/api/**', filters: 'authenticationProcessingFilter,basicAuthenticationFilter,securityContextHolderAwareRequestFilter,anonymousProcessingFilter,basicExceptionTranslationFilter,filterInvocationInterceptor'],
        [pattern: '/**', filters: 'securityContextPersistenceFilter,logoutFilter,authenticationProcessingFilter,securityContextHolderAwareRequestFilter,anonymousProcessingFilter,exceptionTranslationFilter,filterInvocationInterceptor']
]

grails.plugin.springsecurity.securityConfigType = grails.plugin.springsecurity.SecurityConfigType.Requestmap
grails.plugin.springsecurity.cas.active = false
grails.plugin.springsecurity.saml.active = false

// ******************************************************************************
//
//                       +++ INTERCEPT-URL MAP +++
//
// ******************************************************************************
grails.plugin.springsecurity.interceptUrlMap = [
        [pattern: '/', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/login/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/index**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/logout/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/assets/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/resetPassword/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/js/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/css/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/images/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/plugins/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/errors/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/help/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/static/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ext/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/i18n/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/api/**', access: ['ROLE_DETERMINED_DYNAMICALLY']],
        [pattern: '/qapi/**', access: ['ROLE_DETERMINED_DYNAMICALLY']],
        [pattern: '/ssb/survey/**', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M', 'ROLE_SELFSERVICE_BAN_DEFAULT_M']],
        [pattern: '/ssb/userAgreement/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/securityQA/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/selfServiceMenu/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/common/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/menu/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/menu', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/classListApp/**', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-CLASSLISTADMINISTRATOR_BAN_DEFAULT_M']],
        [pattern: '/ssb/classListApp/terms', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-CLASSLISTADMINISTRATOR_BAN_DEFAULT_M']],
        [pattern: '/ssb/dropRoster/**', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M']],
        [pattern: '/ssb/theme/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/themeEditor/**', access: ['ROLE_SELFSERVICE-WTAILORADMIN_BAN_DEFAULT_M']],
        [pattern: '/ssb/keepAlive/data**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/userPreference/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/studentAttendanceTracking/**', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentCommonDashboard/**', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-CLASSLISTADMINISTRATOR_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountSummary', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountSummaryOverview', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountSummaryByTerm', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountSummaryByPeriod', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountDetailByTerm', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountDetailByTermInstallmentPlan', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountDetailByTermFinAid', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountDetailByTermMemo', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountSummary/**', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentAccountPdf', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentAccountPayment', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentTaxNotification', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentHold', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/accountInformation', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentAccountDeposit', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/statementAndPaymentHistory', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/binaryFileDownload', access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/studentAccountStatement',access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/depositProcessing',access: ['ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M']],
        [pattern: '/ssb/paymentSuccess',access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/ssb/paymentFailure',access: ['IS_AUTHENTICATED_ANONYMOUSLY']],

        // FinAid URLs
        [pattern:'/ssb/applicantActiveAidYears/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/awardHistoryAidYears/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/awardHistory/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/awardLetter/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/configurableText/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/federalShoppingSheet/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/financialAid/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/financialAidDashboard/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/queAnsDialogBox/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/resources/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/studentAcademicProgress/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/studentNotifications/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/webTextInfo/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],
        [pattern:'/ssb/withdrawalInformation/**', access:['ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']],

        [pattern: '/ssb/shortcut/**', access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/**', access: ['ROLE_SELFSERVICE-FACULTY_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-CLASSLISTADMINISTRATOR_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-STUDENT_BAN_DEFAULT_M', 'ROLE_SELFSERVICE-FINAID_BAN_DEFAULT_M']]
]

dataSource {
    // configClass = GrailsAnnotationConfiguration.class
    dialect = "org.hibernate.dialect.Oracle10gDialect"
    loggingSql = false
}

hibernate {
    cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
    cache.region_prefix = ''
    //net.sf.ehcache.configurationResourceName = 'financeSSBAppEhCache.xml'
    cache.region.factory_class = 'org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory'
    show_sql = false
    dialect = "org.hibernate.dialect.Oracle10gDialect"
    packagesToScan = "net.hedtech"
    config.location = [
            "classpath:hibernate-banner-core.cfg.xml",
            "classpath:hibernate-banner-finance-validation-common.cfg.xml",
            "classpath:hibernate-banner-general-common.cfg.xml",
            "classpath:hibernate-banner-general-person.cfg.xml",
            "classpath:hibernate-banner-general-validation-common.cfg.xml",
            "classpath:hibernate-banner-student-capp.cfg.xml",
            "classpath:hibernate-banner-student-catalog.cfg.xml",
            "classpath:hibernate-banner-student-common.cfg.xml",
            "classpath:hibernate-banner-student-history.cfg.xml",
            "classpath:hibernate-banner-student-faculty.cfg.xml",
            "classpath:hibernate-banner-student-generalstudent.cfg.xml",
            "classpath:hibernate-banner-student-schedule.cfg.xml",
            "classpath:hibernate-banner-student-validation.cfg.xml",
            "classpath:hibernate-banner-student-registration.cfg.xml",
            "classpath:hibernate-banner-accountsreceivable-common.cfg.xml",
            "classpath:hibernate-banner-accountsreceivable-validation-common.cfg.xml",
            "classpath:hibernate-banner-general-utility.cfg.xml",
            "classpath:hibernate-banner-finaid-common.cfg.xml",
            "classpath:hibernate-banner-finaid-validation.cfg.xml"
    ]
}

defaultResponseHeadersMap = ["x-content-type-options": "nosniff", "X-XSS-Protection": "1; mode=block"]

/*
environments {
    development {
        dataSource {
            url = "jdbc:oracle:thin:@localhost:1521:BAN83"
            driverClassName = "oracle.jdbc.OracleDriver"
            username = "ban_ss_user"
            password = "u_pick_it"

        }
    }
    test {
        dataSource {
            url = "jdbc:oracle:thin:@localhost:1521:BAN83"
            driverClassName = "oracle.jdbc.OracleDriver"
            username = "ban_ss_user"
            password = "u_pick_it"

        }
    }
    production {
        dataSource {
            url = "jdbc:oracle:thin:@localhost:1521:BAN83"
            driverClassName = "oracle.jdbc.OracleDriver"
            username = "ban_ss_user"
            password = "u_pick_it"
        }
    }
}*/
