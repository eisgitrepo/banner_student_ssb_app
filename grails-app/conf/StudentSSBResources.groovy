/*********************************************************************************
 Copyright 2016 - 2018 Ellucian Company L.P. and its affiliates..
 *********************************************************************************/

modules = {
    'studentCommonDashboard' {
        dependsOn "bannerWebLTR, i18n-core, bootstrap, auroraCommon,commonComponents,commonComponentsLTR"
        defaultBundle environment == "development" ? false : "studentCommonDashboard"

        resource url:[plugin: 'sghe-aurora', file: 'css/aurora-header.css'], attrs:[media:'screen, projection']
        resource url: [file: 'css/studentCommon.css'], attrs: [media: 'screen, projection']
    }

    'studentCommonDashboardRTL' {
           dependsOn "bannerWebRTL, i18n-core, bootstrap, auroraCommon,commonComponents,commonComponentsRTL"
           defaultBundle environment == "development" ? false : "studentCommonDashboardRTL"

           resource url:[plugin: 'sghe-aurora', file: 'css/aurora-header-rtl.css'], attrs:[media:'screen, projection']
           resource url: [file: 'css/studentCommon-rtl.css'], attrs: [media: 'screen, projection']
       }

    'commonComponents' {
        resource url:[file: 'js/d3/d3.min.js']
        resource url:[file: 'js/xe-components/xe-ui-components.js']}
    'commonComponentsLTR' {
        resource url:[file: 'css/xe-components/xe-ui-components.min.css']
    }
    'commonComponentsRTL' {
        resource url:[file: 'css/xe-components/xe-ui-components-rtl.min.css']
    }
}
