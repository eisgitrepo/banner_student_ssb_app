/** Copyright 2015-2019 Ellucian Company L.P. and its affiliates. **/

package net.hedtech.banner.student.common

import grails.converters.JSON
import org.springframework.context.i18n.LocaleContextHolder

class StudentSSBTagLib {
    def i18n_setup = { attrs ->
        def map = [:]
        grailsApplication.mainContext.getBean( 'messageSource' ).getMergedBinaryPluginProperties( LocaleContextHolder.getLocale() ).properties.each {key ->
            map.put key.key, key.value
        }
        grailsApplication.mainContext.getBean('messageSource').getMergedProperties(LocaleContextHolder.getLocale()).properties.each { key ->
            map.put key.key, key.value
        }
        out << "window.i18n = ${map as JSON};\n"
    }

    def customStylesheetIncludes = {attrs ->
        def controller = attrs.controller ?: controllerName
        def action = attrs.action ?: actionName

    }

}
