/*******************************************************************************
 Copyright 2016-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/

package net.hedtech.banner.studentssb.system

import grails.gorm.transactions.Transactional
import groovy.sql.Sql
import net.hedtech.banner.menu.SelfServiceMenu
import net.hedtech.banner.service.ServiceBase

@Transactional
class WebTailorService extends ServiceBase {

    //static transactional = true
    def sessionFactory


    public String getTwgParam(String name) {

        def sql = new Sql(sessionFactory.getCurrentSession().connection())
        def sqlQueryString = """SELECT TWGBPARM_PARAM_VALUE pvalue FROM TWGBPARM
                            WHERE TWGBPARM_PARAM_NAME = ${name}"""

        String paramValue = ""
        sql.rows(sqlQueryString).each { t -> paramValue += t.pvalue }
        return paramValue
    }


    public def getTwgMenu(def pidm, String landingPage, def pageValWild) {

        def dataMap = []
        pageValWild = "\'%" + pageValWild + "%\'"
        landingPage = "\'" + landingPage + "\'"
        def sql = new Sql(sessionFactory.getCurrentSession().connection())

        def govroleCriteria
        def govroles = []
        def sqlQuery;
        String pidmCondition = "twgrrole_pidm is NULL"
        if (pidm) {
            pidmCondition = "twgrrole_pidm = " + pidm
            govroles = getGovRole("" + pidm);
            govroleCriteria = getGovRoleCriteria(govroles);
        }
        sqlQuery = "select * " +
                " from twgrmenu a " +
                " where  twgrmenu_enabled = 'Y'" +
                " and (twgrmenu_name in (select twgrwmrl_name from twgrwmrl, twgrrole where " + pidmCondition +
                " and twgrrole_role = twgrwmrl_role and twgrwmrl_name = a.twgrmenu_name) " +
                " or twgrmenu_name in (select twgrwmrl_name from twgrwmrl, govrole " +
                " where govrole_pidm = " + pidm +
                " and  twgrwmrl_role in " + govroleCriteria + "))" +
                " and (Upper(twgrmenu_url) = " + landingPage.toUpperCase() +
                " or  UPPER(twgrmenu_url) like  " + pageValWild.toUpperCase() +")";


        sql.eachRow(sqlQuery) {
            def mnu = new SelfServiceMenu()
            mnu.formName = it.twgrmenu_url
            mnu.pageName = it.twgrmenu_url
            mnu.name = it.twgrmenu_url_text.toUpperCase()
            mnu.url = it.twgrmenu_url
            dataMap.add(mnu)

        };
        return dataMap

    }

    private def getGovRole(String pidm) {
        Sql sql = new Sql(sessionFactory.getCurrentSession().connection())
        def govroles = []
        sql.eachRow("select govrole_student_ind, govrole_alumni_ind, govrole_employee_ind, govrole_faculty_ind, govrole_finance_ind ," +
                "govrole_friend_ind ,govrole_finaid_ind, govrole_bsac_ind from govrole where govrole_pidm = ? ", [pidm]) {
            if (it.govrole_student_ind == "Y") govroles.add("STUDENT")
            if (it.govrole_faculty_ind == "Y") govroles.add("FACULTY")
            if (it.govrole_employee_ind == "Y") govroles.add("EMPLOYEE")
            if (it.govrole_alumni_ind == "Y") govroles.add("ALUMNI")
            if (it.govrole_finance_ind == "Y") govroles.add("FINANCE")
            if (it.govrole_finaid_ind == "Y") govroles.add("FINAID")
            if (it.govrole_friend_ind == "Y") govroles.add("FRIEND")
        }
        return govroles;

    }

    private def getGovRoleCriteria(def govroles) {
        def govroleCriteria
        if (govroles.size() > 0) {

            govroles.each {
                if (it == govroles.first())
                    govroleCriteria = "('" + it.value + "'"
                else
                    govroleCriteria = govroleCriteria + " ,'" + it.value + "'"
            }
            govroleCriteria = govroleCriteria + ")"
        }
        return govroleCriteria;
    }


}
