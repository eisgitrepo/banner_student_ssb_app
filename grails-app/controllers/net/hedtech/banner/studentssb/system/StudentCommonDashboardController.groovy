/*******************************************************************************
 Copyright 2016-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/


package net.hedtech.banner.studentssb.system

import net.hedtech.banner.MessageUtility
import net.hedtech.banner.exceptions.ApplicationException
import net.hedtech.banner.general.person.PersonUtility
import net.hedtech.banner.general.utility.InformationTextUtility
import net.hedtech.banner.general.utility.PreferredNameService
import net.hedtech.banner.security.BannerGrantedAuthorityService
import net.hedtech.banner.studentssb.utils.ConsolidationSessionManager

import java.util.regex.Matcher
import java.util.regex.Pattern

class StudentCommonDashboardController {


    static defaultAction = 'dashboard'
    def springSecurityService
    private static final String STUDENT_LAND_PAGE_NAME = 'STUDENTLANDINGPAGE'
    PreferredNameService preferredNameService
    def webTailorService

    def dashboard() {
        if (!validateRequest()) return

        def personDetail
        boolean isClassListAdmin = BannerGrantedAuthorityService.getAuthorities()?.objectName?.contains( ConsolidationSessionManager.CLASSLIST_ADMINISTRATOR_ROLE )
        session.setAttribute( 'isAdminUser', isClassListAdmin )

        String landingPage = webTailorService.getTwgParam(ConsolidationSessionManager.LANDING_PAGE)
        if(!landingPage){
            throw new ApplicationException( "", MessageUtility.message("student.wtailor.parameter"))
        }
        if (isLoggedInFaculty() || isLoggedInStudent() || isLoggedInFinAidSS()) {
            personDetail = PersonUtility.getPerson(springSecurityService.getAuthentication().user.pidm)?.collect {
                ['firstName': it.firstName, 'lastName': it.lastName, 'pidm': it.pidm, 'id': it.id, 'surnamePrefix': it.surnamePrefix, 'bannerId': it.bannerId]
            }[0]

            accessStudenDashboard(landingPage) ? getDisplayDashboard(personDetail) :
                    redirectToRoleBasedPage(personDetail, landingPage)

        } else if(isClassListAdmin) {
            redirect controller: "classListApp", action: "classListPage"
            return true
        } else {
            response.sendError(403)
            return
        }
    }


    private String getPreferredName(def pidm) {
        Map<String, String> params = null;
        String pageName = 'Landing Page';
        String productName = 'Student';
        String appName = 'Student Self-Service';
        params = new HashMap<String, String>();
        params.put("pidm", pidm);
        params.put("productname", productName);
        params.put("appname", appName);
        params.put("pagename", pageName);
        return preferredNameService.getPreferredName(params);
    }

    public static boolean isLoggedInFaculty() {
        def userAuthorities = BannerGrantedAuthorityService.getAuthorities()?.objectName
        return userAuthorities?.contains(ConsolidationSessionManager.FACULTY_ROLE)
    }

    public static boolean isLoggedInStudent() {
        def userAuthorities = BannerGrantedAuthorityService.getAuthorities()?.objectName
        return userAuthorities?.contains(ConsolidationSessionManager.STUDENT_ROLE)
    }

    public static boolean isLoggedInFinAidSS() {
        def userAuthorities = BannerGrantedAuthorityService.getAuthorities()?.objectName
        return userAuthorities?.contains(ConsolidationSessionManager.FINAID_ROLE)
    }

    public static Integer getLoggedInUserPidm() {
        return BannerGrantedAuthorityService.getPidm()
    }

    private boolean validateRequest() {
        if (!getLoggedInUserPidm()) {
            /* unauthorized */
            response.sendError(401)
            return false
        }
        return true
    }

    private accessStudenDashboard(String landingPage) {
        return (ConsolidationSessionManager.Page.studentCommonDashboard.toString().equals(landingPage))
    }


    private getDisplayDashboard(personDetail) {
        String preferredName = null
        preferredName = getPreferredName(personDetail.pidm)
        if (!preferredName) {
            preferredName = personDetail.firstName + ' ' + personDetail.lastName
        }
        //Multiple message
        Map informationTexts = new HashMap<String, String>()
        informationTexts = InformationTextUtility.getMessages(STUDENT_LAND_PAGE_NAME)
        def headerText = informationTexts['student.landpage.header']
        def greetingText = informationTexts['student.landpage.greeting']
        def infoText = informationTexts['student.landpage.text']
        def model = [preferredName: preferredName, headerText: headerText, greetingText: greetingText, infoText: infoText, bannerId: personDetail.bannerId]
        render model: model, view: "studentCommonDashboard"

    }

    private redirectToRoleBasedPage(personDetail, landingPage) {
        String pageName
        List menuList
        if (!checkUrl(landingPage)) {
            throw new ApplicationException("", MessageUtility.message("student.wtailor.url"))

        } else {
            pageName = landingPage.substring(landingPage.lastIndexOf('/') + 1)
            if (landingPage && pageName) {
                //removing the leading and trailing spaces from the url
                //TODO:Fix grails332 issue in weblogic - webTailorService returns null.
                menuList = webTailorService.getTwgMenu(personDetail.pidm, landingPage.trim(), pageName.trim())
            }
            (menuList && menuList.size() > 0) ? redirect(url: landingPage) : getDisplayDashboard(personDetail)
        }

    }

    private boolean checkUrl(landingPage) {

        String http = "((http:\\/\\/|https:\\/\\/)?(www.)?(([a-zA-Z0-9-]){2,}\\.){1,4}([a-zA-Z]){2,6}(\\/([a-zA-Z-_\\/\\.0-9#:?=&;,]*)?)?)";
        Pattern pattern = Pattern.compile(http);
        Matcher matcher = pattern.matcher(landingPage);
        while (matcher.find()) {
            return true
        }
    }

}