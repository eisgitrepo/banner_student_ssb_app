/*******************************************************************************
 Copyright 2016-2018 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package net.hedtech.banner.studentssb.system

import net.hedtech.banner.general.person.PersonIdentificationName
import net.hedtech.banner.general.person.PersonPictureBaseController
import net.hedtech.banner.general.person.PersonUtility
import net.hedtech.banner.security.BannerGrantedAuthorityService
import net.hedtech.banner.student.StudentSsbControllerUtility
import net.hedtech.banner.studentssb.utils.ConsolidationSessionManager


class LandingPagePictureController extends PersonPictureBaseController {

    boolean hasAccess() {
        if(!params.bannerId) {
            return false
        }

        if(!LandingPagePictureController.isLoggedInFacultyOrStudent()) {
            return false
        }

        def person = PersonUtility.getPerson(getLoggedInUserPidm())
        if(person?.bannerId == params.bannerId) {
            return true
        }
        return false
    }


    public static boolean isLoggedInFacultyOrStudent() {
        def userAuthorities = BannerGrantedAuthorityService.getAuthorities()?.objectName
        Boolean isFaculty = userAuthorities?.contains(ConsolidationSessionManager.FACULTY_ROLE)
        Boolean isStudent = userAuthorities?.contains(ConsolidationSessionManager.STUDENT_ROLE)
        if (isFaculty || isStudent) {
            return true
        }
        return false
    }

    private static Integer getLoggedInUserPidm() {
        StudentSsbControllerUtility.pidm
    }
}
