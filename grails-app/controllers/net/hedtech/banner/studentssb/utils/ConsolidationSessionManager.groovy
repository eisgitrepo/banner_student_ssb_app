/*******************************************************************************
 Copyright 2016-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/

package net.hedtech.banner.studentssb.utils

class ConsolidationSessionManager {

    public static final String FACULTY_ROLE = "SELFSERVICE-FACULTY"
    public static final String STUDENT_ROLE = "SELFSERVICE-STUDENT"
    public static final String FINAID_ROLE = "SELFSERVICE-FINAID"
    public static final String LANDING_PAGE = "STU_SS_LAND_PAGE"
    public static final String LANDING_PAGE_TYPE = "STU_SS_LAND_PAGE_TYP"
    public static final String CLASSLIST_ADMINISTRATOR_ROLE = "SELFSERVICE-CLASSLISTADMINISTRATOR"

    public enum Page {
        studentCommonDashboard
    }

}
