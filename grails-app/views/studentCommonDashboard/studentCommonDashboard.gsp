<%@ page contentType="text/html;charset=UTF-8" defaultCodec="none" %>
<!DOCTYPE html>
<%--
/*******************************************************************************
Copyright 2015-2019 Ellucian Company L.P. and its affiliates.
*******************************************************************************/
--%>
<html>
<head>
    <g:applyLayout name="bannerWebPage">
        <title>
            <g:message code="student.dashboard.pageTitle"/>
        </title>
        <meta name="menuEndPoint" content="${request.contextPath}/ssb/menu"/>
        <meta name="menuBaseURL" content="${request.contextPath}/ssb"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <!-- <META HTTP-EQUIV="X-Frame-Options" CONTENT="deny">-->
        <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no, user-scalable=0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <g:if test="${message(code: 'default.language.direction') == 'rtl'}">
            <asset:javascript src="modules/studentCommonDashboardRTL-mf.js"/>
            <asset:stylesheet src="modules/studentCommonDashboardRTL-mf.css"/>
            <%--<r:require modules="studentCommonDashboardRTL"/>--%>
        </g:if>
        <g:else>
           <%-- <r:require modules="studentCommonDashboard"/>--%>
            <asset:javascript src="modules/studentCommonDashboard-mf.js"/>
            <asset:stylesheet src="modules/studentCommonDashboard-mf.css"/>
        </g:else>

    </g:applyLayout>


    <g:bannerMessages/>
    <!--[if lte IE 8]>
      <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');
      </script>
    <![endif]-->
</head>

<body>
    <div id="content">
        <div class="container">
            <h1 class="marBottom20px">${headerText}</h1>
            <div class="row padTop20px">
                <div id="getPersonImage">
                    <img class="img-circle landing-page-image"
                         src="${createLink(uri: '/ssb/landingPagePicture/picture', params: [bannerId: "${bannerId}"])}"
                         alt="${bannerId}">
                </div>
                <div class="heading-txt">
                    <p>${greetingText}  ${preferredName},<br/>
						"${infoText}"
                       
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script type="text/javascript">
    function showPage(element) {
        window.location.href=$(element).attr('data-url');
    }
    $(document).ready(function(){
        $( "#breadcrumb-panel").remove();
    });

</script>


