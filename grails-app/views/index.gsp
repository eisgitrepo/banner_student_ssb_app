<%--
/*******************************************************************************
Copyright 2009-2019 Ellucian Company L.P. and its affiliates.
*******************************************************************************/
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <g:set var="mep" value="${params?.mepCode}"/>
    <g:set var="hideSSBHeaderComps" value="${params?.hideSSBHeaderComps}" />

    <g:if test="${mep && hideSSBHeaderComps}">
        <g:set var="url" value="${'ssb/studentProfile?mepCode='+mep+'&hideSSBHeaderComps='+hideSSBHeaderComps}" />
    </g:if>
    <g:elseif test="${mep}">
        <g:set var="url" value="${'ssb/studentProfile?mepCode='+mep}" />
    </g:elseif>
    <g:elseif test="${hideSSBHeaderComps}">
        <g:set var="url" value="${'ssb/studentProfile?hideSSBHeaderComps='+hideSSBHeaderComps}" />
    </g:elseif>
    <g:else>
        <g:set var="url" value="${'ssb/studentProfile'}" />
    </g:else>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    %{--<g:set var="appName" value="${System.properties['BANNERXE_APP_NAME']}"/>--}%
   %{-- <g:if test="${appName.equals( 'StudentATTR' )}">
        <%
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> StudentATTR Lokee BANNERXE_APP_NAME is "+System.properties['BANNERXE_APP_NAME'])
        %>
        <meta HTTP-EQUIV="REFRESH" content="0; url=${!mep ? 'ssb/studentAttendanceTracking' : '/ssb/studentAttendanceTracking?mepCode='+mep}">
    </g:if>
    <g:elseif test="${appName.equals( 'StudentSSB' )}">

        <%
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> StudentSSB Lokee BANNERXE_APP_NAME is "+System.properties['BANNERXE_APP_NAME'])
        %>
        <meta HTTP-EQUIV="REFRESH" content="0; url=${url}">
    </g:elseif>
    <g:else>


        <%
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Default Lokee BANNERXE_APP_NAME is "+System.properties['BANNERXE_APP_NAME'])
        %>--}%


        <meta HTTP-EQUIV="REFRESH" content="0; url=${!mep ? 'ssb/studentCommonDashboard' : 'ssb/studentCommonDashboard?mepCode='+mep}">


%{--    </g:else>--}%

</head>
<body>
</body>
</html>
