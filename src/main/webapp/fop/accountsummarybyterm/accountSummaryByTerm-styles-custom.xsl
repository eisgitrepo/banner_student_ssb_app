<?xml version="1.0" encoding="UTF-8" ?>
<!-- Copyright 2018 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl">

    <!--accordion- description-->
    <xsl:attribute-set name="accordion-description">
        <xsl:attribute name="height">24px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <!--accordion- Amount display-->
    <xsl:attribute-set name="accordion-amountDisplay">
        <xsl:attribute name="height">24px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <!--accordion- Amount info display-->
    <xsl:attribute-set name="accordion-amountInfoDisplay">
        <xsl:attribute name="height">18px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">12px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.5</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
        <xsl:attribute name="padding-bottom">15pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- accordionInfo. -->
    <xsl:attribute-set name="accordionInfo">
        <xsl:attribute name="border-top">2pt solid #51abff</xsl:attribute>
        <xsl:attribute name="background-color">#eff7ff</xsl:attribute>
        <xsl:attribute name="height">72px</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="accordion">
        <xsl:attribute name="padding-top">24px</xsl:attribute>
        <xsl:attribute name="padding-bottom">24px</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="divider">
        <xsl:attribute name="border-top">1pt solid #5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="totalRow">
        <xsl:attribute name="border-bottom">1pt #b2b3b7</xsl:attribute>
    </xsl:attribute-set>


</xsl:stylesheet>
