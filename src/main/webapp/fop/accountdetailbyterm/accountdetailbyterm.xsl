<?xml version="1.0" encoding="UTF-16"?>
<!-- Copyright 2018 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl fo">

    <xsl:include href="../common/common-config.xsl"/>
    <xsl:include href="../styles/accountSummary-styles.xsl"/>
    <xsl:include href="accountDetailByTerm-styles-custom.xsl"/>
    <xsl:template match="pdfModel">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="accountSummaryOverview-page"
                                       xsl:use-attribute-sets="page">
                    <fo:region-body xsl:use-attribute-sets="body-region"/>
                    <fo:region-before xsl:use-attribute-sets="header-region"/>
                    <fo:region-after xsl:use-attribute-sets="footer-region"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="accountSummaryOverview-page">

                <fo:static-content flow-name="xsl-region-before">
                    <fo:block-container xsl:use-attribute-sets="container pageHeader">
                        <fo:block>
                            <fo:table>
                                <fo:table-column column-width="25%"/>
                                <fo:table-column column-width="75%"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        <fo:table-cell>
                                            <fo:block>
                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                        and $base-writing-mode = 'lr')
                                                        or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                        and $base-writing-mode = 'rl')">
                                                    <xsl:choose>
                                                        <xsl:when test="logoFile/svgFile = 'false'">
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="accountSummaryOverview-logo"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <fo:instream-foreign-object>
                                                                <svg:svg width="150" height="80"
                                                                         xmlns:svg="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <svg:g>
                                                                        <svg:image x="0" y="0" width="140px"
                                                                                   height="60px">
                                                                            <xsl:attribute name="xlink:href">
                                                                                <xsl:value-of
                                                                                        select="logoFile/logoFileLocation"/>
                                                                            </xsl:attribute>
                                                                        </svg:image>
                                                                    </svg:g>
                                                                </svg:svg>
                                                            </fo:instream-foreign-object>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <fo:block-container xsl:use-attribute-sets="container">

                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                                                                    and $base-writing-mode = 'lr')
                                                                                                    or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                                                                    and $base-writing-mode = 'rl') ">

                                                    <fo:block xsl:use-attribute-sets="userInfo numeric-align"
                                                              width="auto">
                                                        <fo:block>
                                                            <xsl:value-of select="accountDetailByTerm/fullName"/>
                                                        </fo:block>
                                                        <br/>
                                                        <fo:block xsl:use-attribute-sets="userName">
                                                            <xsl:value-of select="accountDetailByTerm/username"/>
                                                        </fo:block>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="clientDate numeric-align"
                                                              width="auto">
                                                        <xsl:value-of select="accountDetailByTerm/clientDate"/>
                                                    </fo:block>
                                                </xsl:if>

                                            </fo:block-container>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container xsl:use-attribute-sets="container accountSummaryOverview-table">
                        <fo:block-container xsl:use-attribute-sets="pageTitle">
                            <fo:block text-align='left'>
                                <xsl:value-of select="exsl:node-set($labels)/title"/>
                            </fo:block>
                        </fo:block-container>
                        <xsl:if test="accountDetailByTerm/hasData='false'">
                            <fo:block-container xsl:use-attribute-sets="pageInfo">

                                <fo:block
                                        xsl:use-attribute-sets="pageInfoText">
                                    &#160;&#160;<fo:external-graphic
                                        xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160;
                                    &#160;<xsl:value-of select="accountDetailByTerm/pageInfoText"/>
                                </fo:block>

                            </fo:block-container>
                        </xsl:if>
                        <fo:block>
                            <xsl:for-each select="accountDetailByTerm/sections">
                                <fo:table>
                                    <fo:table-column column-width="75%"/>
                                    <fo:table-column column-width="25%"/>
                                    <fo:table-body>
                                        <fo:table-row>
                                            <fo:table-cell number-columns-spanned="2">
                                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                        <xsl:if test="sectionCode!='TD'">
                                            <xsl:choose>
                                                <xsl:when
                                                        test="sectionCode='IP'">
                                                    <fo:table-row>
                                                        <fo:table-cell>
                                                            <fo:block xsl:use-attribute-sets="accordion-description">
                                                                <fo:bidi-override unicode-bidi="embed"
                                                                                  direction="ltr">
                                                                    <xsl:value-of select="headerLabel"/>
                                                                </fo:bidi-override>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                        <fo:table-cell>
                                                            <fo:block
                                                                    xsl:use-attribute-sets="accordion-amountDisplay numeric-align">
                                                                <xsl:value-of select="headerAmountDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <fo:table-row>
                                                        <fo:table-cell>
                                                            <fo:block>
                                                                <fo:table>
                                                                    <xsl:choose>
                                                                        <xsl:when
                                                                                test="exsl:node-set($config)/languageDirection = 'rtl'">
                                                                            <xsl:if test="sectionCode='FA'">
                                                                                <fo:table-column column-width="28%"/>
                                                                                <fo:table-column column-width="7%"/>
                                                                                <fo:table-column column-width="65%"/>
                                                                            </xsl:if>
                                                                            <xsl:if test="sectionCode='MO'">
                                                                                <fo:table-column column-width="10%"/>
                                                                                <fo:table-column column-width="7%"/>
                                                                                <fo:table-column column-width="83%"/>
                                                                            </xsl:if>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:if test="sectionCode='FA'">
                                                                                <fo:table-column column-width="39%"/>
                                                                                <fo:table-column column-width="9%"/>
                                                                                <fo:table-column column-width="57%"/>
                                                                            </xsl:if>
                                                                            <xsl:if test="sectionCode='MO'">
                                                                                <fo:table-column column-width="12.5%"/>
                                                                                <fo:table-column column-width="9%"/>
                                                                                <fo:table-column column-width="80.5%"/>
                                                                            </xsl:if>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>

                                                                    <fo:table-body>
                                                                        <fo:table-row>
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="zero-margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="accordion-description">
                                                                                    <fo:bidi-override
                                                                                            unicode-bidi="embed"
                                                                                            direction="ltr">
                                                                                        <xsl:value-of
                                                                                                select="headerLabel"/>
                                                                                    </fo:bidi-override>
                                                                                </fo:block>
                                                                            </fo:table-cell>

                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="zero-margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="accordion-description">
                                                                                    <xsl:value-of
                                                                                            select="exsl:node-set($labels)/asOf"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="zero-margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="accordion-description">
                                                                                    <xsl:value-of
                                                                                            select="../config/asOfDate"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </fo:table-row>
                                                                    </fo:table-body>
                                                                </fo:table>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="numeric-align">
                                                            <fo:block>
                                                                <xsl:value-of
                                                                        select="headerAmountDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:if>
                                        <fo:table-row>
                                            <fo:table-cell number-columns-spanned="2">
                                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                        <xsl:choose>
                                            <xsl:when test="hasData='false'">
                                                <fo:table-row>
                                                    <fo:table-cell number-columns-spanned="2"
                                                                   xsl:use-attribute-sets="pageInfo">
                                                        <fo:block
                                                                xsl:use-attribute-sets="pageInfoText">
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="accountSummary-info-circle-icon"/>
                                                            &#160;
                                                            &#160;
                                                            <fo:bidi-override unicode-bidi="embed"
                                                                              direction="ltr">
                                                                <xsl:value-of
                                                                        select="sectionInfoMsg"/>
                                                            </fo:bidi-override>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <fo:table-row>
                                                    <fo:table-cell number-columns-spanned="2">
                                                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <xsl:if test="sectionCode='TD'">
                                                    <fo:table-row number-columns-spanned="2">
                                                        <fo:table-cell>
                                                            <fo:block
                                                                    xsl:use-attribute-sets="accordion-description">
                                                                <fo:bidi-override unicode-bidi="embed"
                                                                                  direction="ltr">
                                                                    <xsl:value-of select="headerLabel"/>
                                                                </fo:bidi-override>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                    <fo:table-row>
                                                        <fo:table-cell number-columns-spanned="2">
                                                            <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </xsl:if>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <fo:table-row
                                                        xsl:use-attribute-sets="pageInfo pageInfoText">
                                                    <fo:table-cell number-columns-spanned="2">
                                                        <fo:block
                                                                xsl:use-attribute-sets="pageInfoText">
                                                            &#160;&#160;&#160;
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="accountSummary-info-circle-icon"/>
                                                            &#160;
                                                            &#160;
                                                            <fo:bidi-override unicode-bidi="embed"
                                                                              direction="ltr">
                                                                <xsl:value-of
                                                                        select="sectionInfoMsg"/>
                                                            </fo:bidi-override>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <fo:table-row>
                                                    <fo:table-cell number-columns-spanned="2">
                                                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <xsl:if test="sectionCode='TD'">
                                                    <fo:table-row number-columns-spanned="2">
                                                        <fo:table-cell>
                                                            <fo:block xsl:use-attribute-sets="accordion-description">
                                                                <fo:bidi-override unicode-bidi="embed"
                                                                                  direction="ltr">
                                                                    <xsl:value-of select="headerLabel"/>
                                                                </fo:bidi-override>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                    <fo:table-row>
                                                        <fo:table-cell number-columns-spanned="2">
                                                            <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </xsl:if>
                                                <xsl:if test="gridConfig/showGrid='true'">
                                                    <fo:table-row>
                                                        <fo:table-cell number-columns-spanned="2">
                                                            <fo:table xsl:use-attribute-sets="table">
                                                                <fo:table-header>
                                                                    <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                        <xsl:if test="gridConfig/showDefaultCode!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header ">
                                                                                    <xsl:value-of
                                                                                            select="defaultCodeLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showDate!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header">
                                                                                    <xsl:value-of
                                                                                            select="dateLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showStudyPath!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header ">
                                                                                    <xsl:value-of
                                                                                            select="studyPathNameLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showDescription!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header ">
                                                                                    <xsl:value-of
                                                                                            select="descriptionLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showCharge!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header numeric-align">
                                                                                    <xsl:value-of
                                                                                            select="chargeLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showPayment!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-column-header numeric-align">
                                                                                    <xsl:value-of
                                                                                            select="paymentLabel"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                    </fo:table-row>
                                                                </fo:table-header>
                                                                <fo:table-body>
                                                                    <xsl:for-each
                                                                            select="gridData/result">
                                                                        <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                            <xsl:if test="../../gridConfig/showDefaultCode!='false'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="detailCode"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                            <xsl:if test="../../gridConfig/showDate='true'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="date"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                            <xsl:if test="../../gridConfig/showStudyPath!='false'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="studyPathName"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                            <xsl:if test="../../gridConfig/showDescription!='false'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="description"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                            <xsl:if test="../../gridConfig/showCharge!='false'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="chargeDisplay"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                            <xsl:if test="../../gridConfig/showPayment!='false'">
                                                                                <fo:table-cell
                                                                                        xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="margin">
                                                                                        <xsl:value-of
                                                                                                select="paymentDisplay"/>
                                                                                    </fo:block>
                                                                                </fo:table-cell>
                                                                            </xsl:if>
                                                                        </fo:table-row>
                                                                    </xsl:for-each>
                                                                    <!--Total-->
                                                                    <fo:table-row
                                                                            xsl:use-attribute-sets="tableRow totalRow">
                                                                        <xsl:if test="gridConfig/showDefaultCode='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="table-row-header">
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showDescription='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                                                <xsl:if test="gridConfig/showTotalRow='true'">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="table-row-header">
                                                                                        <xsl:value-of
                                                                                                select="exsl:node-set($labels)/totalRow"/>
                                                                                    </fo:block>
                                                                                </xsl:if>
                                                                                <xsl:if test="gridConfig/showTotalRow='false'">
                                                                                    <fo:block
                                                                                            xsl:use-attribute-sets="table-row-header">
                                                                                    </fo:block>
                                                                                </xsl:if>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showDate!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>

                                                                        <xsl:if test="gridConfig/showCharge!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="gridData/totals/totalchargeDisplay"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="gridConfig/showPayment!='false'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="gridData/totals/totalpaymentDisplay"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                    </fo:table-row>
                                                                </fo:table-body>
                                                            </fo:table>
                                                        </fo:table-cell>
                                                    </fo:table-row>
                                                </xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <fo:table-row>
                                            <fo:table-cell number-columns-spanned="2">
                                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                        <xsl:for-each select="calculation">
                                            <xsl:if test="headerAmount='false'">
                                                <xsl:if test="show='true'">
                                                    <fo:table-row>
                                                        <fo:table-cell>
                                                            <xsl:choose>
                                                                <xsl:when
                                                                        test="hasAsOfDate='true'">
                                                                    <fo:block
                                                                            xsl:use-attribute-sets="calculation-description margin">
                                                                        <fo:table>
                                                                            <xsl:choose>
                                                                                <xsl:when
                                                                                        test="exsl:node-set($config)/languageDirection = 'rtl'">
                                                                                    <fo:table-column
                                                                                            column-width="18%"/>
                                                                                    <fo:table-column column-width="5%"/>
                                                                                    <fo:table-column
                                                                                            column-width="77%"/>
                                                                                </xsl:when>
                                                                                <xsl:otherwise>
                                                                                    <fo:table-column
                                                                                            column-width="25.5%"/>
                                                                                    <fo:table-column column-width="7%"/>
                                                                                    <fo:table-column
                                                                                            column-width="67.5%"/>
                                                                                </xsl:otherwise>
                                                                            </xsl:choose>

                                                                            <fo:table-body>
                                                                                <fo:table-row
                                                                                        xsl:use-attribute-sets="zero-margin">
                                                                                    <fo:table-cell
                                                                                            xsl:use-attribute-sets="zero-margin">
                                                                                        <fo:block
                                                                                                xsl:use-attribute-sets="zero-margin">
                                                                                            <xsl:value-of
                                                                                                    select="label"/>
                                                                                        </fo:block>
                                                                                    </fo:table-cell>

                                                                                    <fo:table-cell
                                                                                            xsl:use-attribute-sets="zero-margin">
                                                                                        <fo:block
                                                                                                xsl:use-attribute-sets="zero-margin">
                                                                                            <xsl:value-of
                                                                                                    select="exsl:node-set($labels)/asOf"/>
                                                                                        </fo:block>
                                                                                    </fo:table-cell>
                                                                                    <fo:table-cell
                                                                                            xsl:use-attribute-sets="zero-margin">
                                                                                        <fo:block
                                                                                                xsl:use-attribute-sets="zero-margin">
                                                                                            <xsl:value-of
                                                                                                    select="asOfDate"/>
                                                                                        </fo:block>
                                                                                    </fo:table-cell>

                                                                                </fo:table-row>
                                                                            </fo:table-body>
                                                                        </fo:table>
                                                                    </fo:block>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <fo:block
                                                                            xsl:use-attribute-sets="calculation-description margin">
                                                                        <xsl:value-of
                                                                                select="label"/>
                                                                    </fo:block>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </fo:table-cell>
                                                        <fo:table-cell>
                                                            <fo:block
                                                                    xsl:use-attribute-sets="calculation-amount margin numeric-align">
                                                                <xsl:value-of
                                                                        select="value"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </fo:table-row>

                                                    <xsl:choose>
                                                        <xsl:when test="infoText!=''">
                                                            <xsl:if test="showInfoText='true'">
                                                                <fo:table-row>
                                                                    <fo:table-cell number-columns-spanned="2">
                                                                        <fo:block
                                                                                xsl:use-attribute-sets="calculation-amount margin calculationInfoText">
                                                                            <xsl:value-of
                                                                                    select="infoText"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </fo:table-row>
                                                            </xsl:if>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <fo:table-row>
                                                                <fo:table-cell number-columns-spanned="2">
                                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                            </xsl:if>
                                        </xsl:for-each>
                                        <fo:table-row>
                                            <fo:table-cell number-columns-spanned="2">
                                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-body>
                                </fo:table>
                            </xsl:for-each>
                        </fo:block>

                    </fo:block-container>
                    <fo:block-container xsl:use-attribute-sets="container disclaimer">
                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                        </fo:block>
                        <xsl:if test="exsl:node-set($config)/custom/displayDisclaimerInfoOnPrintPreview = 'true'">
                            <fo:block xsl:use-attribute-sets="disclaimerHeader">
                                <xsl:value-of select="exsl:node-set($labels)/disclaimer"/>
                            </fo:block>
                            <fo:block xsl:use-attribute-sets="disclaimerMessage">
                                <fo:bidi-override unicode-bidi="embed"
                                                  direction="ltr">
                                    <xsl:value-of select="accountDetailByTerm/disclaimerMessage"/>
                                </fo:bidi-override>
                            </fo:block>
                        </xsl:if>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
