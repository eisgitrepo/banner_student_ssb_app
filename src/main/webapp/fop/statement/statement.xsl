<?xml version="1.0" encoding="UTF-16"?>
<!-- Copyright 2019 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl fo">

    <xsl:include href="../common/common-config.xsl"/>
    <xsl:include href="../styles/accountSummary-styles.xsl"/>
    <xsl:include href="statement-styles-custom.xsl"/>
    <xsl:template match="pdfModel">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="accountSummaryOverview-page"
                                       xsl:use-attribute-sets="page">
                    <fo:region-body xsl:use-attribute-sets="body-region"/>
                    <fo:region-before xsl:use-attribute-sets="header-region"/>
                    <fo:region-after xsl:use-attribute-sets="footer-region"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="accountSummaryOverview-page">

                <fo:static-content flow-name="xsl-region-before">
                    <fo:block-container xsl:use-attribute-sets="container pageHeader">
                        <fo:block>
                            <fo:table>
                                <fo:table-column column-width="50%"/>
                                <fo:table-column column-width="50%"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        <fo:table-cell>
                                            <fo:block>
                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                        and $base-writing-mode = 'lr')
                                                        or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                        and $base-writing-mode = 'rl')">
                                                    <xsl:choose>
                                                        <xsl:when test="logoFile/svgFile = 'false'">
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="accountSummaryOverview-logo"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <fo:instream-foreign-object>
                                                                <svg:svg width="150" height="80"
                                                                         xmlns:svg="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <svg:g>
                                                                        <svg:image x="0" y="0" width="140px"
                                                                                   height="60px">
                                                                            <xsl:attribute name="xlink:href">
                                                                                <xsl:value-of
                                                                                        select="logoFile/logoFileLocation"/>
                                                                            </xsl:attribute>
                                                                        </svg:image>
                                                                    </svg:g>
                                                                </svg:svg>
                                                            </fo:instream-foreign-object>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <fo:block-container xsl:use-attribute-sets="container">

                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT' and $base-writing-mode = 'lr')
                                                                                                             or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                                                                             and $base-writing-mode = 'rl') ">

                                                    <fo:block xsl:use-attribute-sets="userInfo numeric-align"
                                                              width="auto">
                                                        <fo:block>
                                                            <xsl:value-of select="statement/fullName"/>
                                                        </fo:block>
                                                        <br/>
                                                        <fo:block xsl:use-attribute-sets="userName">
                                                            <xsl:value-of select="statement/username"/>
                                                        </fo:block>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="clientDate numeric-align"
                                                              width="auto">
                                                        <xsl:value-of select="statement/clientDate"/>
                                                    </fo:block>
                                                </xsl:if>
                                            </fo:block-container>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container xsl:use-attribute-sets="container accountSummaryOverview-table">
                        <fo:block-container xsl:use-attribute-sets="pageTitle">
                            <fo:block text-align='left'>
                                <xsl:value-of select="exsl:node-set($labels)/title"/>
                            </fo:block>
                        </fo:block-container>
                        <fo:block-container xsl:use-attribute-sets="pageInfo">
                            <fo:block xsl:use-attribute-sets="pageInfoText">
                                <fo:external-graphic
                                        xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                <fo:bidi-override
                                        unicode-bidi="embed"
                                        direction="ltr">
                                    <xsl:value-of
                                            select="statement/infoMsg"/>&#160; <xsl:value-of select="statement/cutOffDate"/>
                                </fo:bidi-override>
                            </fo:block>
                        </fo:block-container>
                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                        </fo:block>
                        <xsl:if test="exsl:node-set($config)/custom/showCurrentAmountDue='true'">
                            <fo:block xsl:use-attribute-sets="line-break">&#160;
                            </fo:block>
                        </xsl:if>
                    </fo:block-container>
                    <fo:block-container xsl:use-attribute-sets="container disclaimer">
                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                        </fo:block>
                        <xsl:if test="exsl:node-set($config)/custom/displayDisclaimerInfoOnPrintPreview = 'true'">
                            <fo:block xsl:use-attribute-sets="disclaimerHeader">
                                <xsl:value-of select="exsl:node-set($labels)/disclaimer"/>
                            </fo:block>
                            <fo:block xsl:use-attribute-sets="disclaimerMessage">
                                <xsl:value-of select="statement/disclaimerMessage"/>
                            </fo:block>
                        </xsl:if>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
