<?xml version="1.0" encoding="UTF-16"?>
<!-- Copyright 2019 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl fo">

    <xsl:include href="../common/common-config.xsl"/>
    <xsl:include href="studentAccountTaxNotification-styles.xsl"/>
    <xsl:template match="pdfModel">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="studentAccountTaxNotification-page"
                                       xsl:use-attribute-sets="page">
                    <fo:region-body xsl:use-attribute-sets="body-region"/>
                    <fo:region-before xsl:use-attribute-sets="header-region"/>
                    <fo:region-after xsl:use-attribute-sets="footer-region"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="studentAccountTaxNotification-page">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block-container xsl:use-attribute-sets="container pageHeader">
                        <fo:block>
                            <fo:table>
                                <fo:table-column column-width="25%"/>
                                <fo:table-column column-width="75%"/>
                                <fo:table-body>
                                    <fo:table-row height="80px">
                                        <fo:table-cell>
                                            <fo:block>
                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                        and $base-writing-mode = 'lr')
                                                        or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                        and $base-writing-mode = 'rl')">
                                                    <xsl:choose>
                                                        <xsl:when test="logoFile/svgFile = 'false'">
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="taxNotification-logo"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <fo:instream-foreign-object>
                                                                <svg:svg width="150" height="80"
                                                                         xmlns:svg="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <svg:g>
                                                                        <svg:image x="0" y="0" width="140px"
                                                                                   height="60px">
                                                                            <xsl:attribute name="xlink:href">
                                                                                <xsl:value-of
                                                                                        select="logoFile/logoFileLocation"/>
                                                                            </xsl:attribute>
                                                                        </svg:image>
                                                                    </svg:g>
                                                                </svg:svg>
                                                            </fo:instream-foreign-object>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <fo:block-container xsl:use-attribute-sets="container">

                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                                                                    and $base-writing-mode = 'lr')
                                                                                                    or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                                                                    and $base-writing-mode = 'rl') ">

                                                    <fo:block xsl:use-attribute-sets="userInfo numeric-align"
                                                              width="auto">
                                                        <fo:block>
                                                            <xsl:value-of
                                                                    select="studentAccountTaxNotification/fullName"/>
                                                        </fo:block>
                                                        <xsl:if test="studentAccountTaxNotification/displayUserName='true'">
                                                            <fo:block xsl:use-attribute-sets="userName numeric-align">
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/username"/>
                                                            </fo:block>
                                                        </xsl:if>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="clientDate numeric-align"
                                                              width="auto">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/clientDate"/>
                                                    </fo:block>
                                                </xsl:if>

                                            </fo:block-container>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container xsl:use-attribute-sets="container taxNotification-table">
                        <fo:block-container xsl:use-attribute-sets="pageTitle">
                            <fo:block text-align='left'>
                                <xsl:value-of select="exsl:node-set($labels)/title"/>
                            </fo:block>
                        </fo:block-container>
                        <fo:block-container xsl:use-attribute-sets="pageInfo">
                            <fo:block xsl:use-attribute-sets="pageInfoText">
                                <fo:external-graphic
                                        xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                <fo:bidi-override
                                        unicode-bidi="embed"
                                        direction="ltr">
                                    <xsl:value-of
                                            select="studentAccountTaxNotification/pageInfoText"/>
                                </fo:bidi-override>
                            </fo:block>
                        </fo:block-container>
                        <fo:block xsl:use-attribute-sets="line-break">&#160;</fo:block>
                        <xsl:if test="studentAccountTaxNotification/config/formSection/showForm='true'">
                            <fo:block-container xsl:use-attribute-sets="noRTL">
                               <xsl:if test="studentAccountTaxNotification/config/formSection/showWaterMark='true'">
                                    <fo:block-container top="3in" left="0in"
                                                        absolute-position="absolute">
                                        <fo:block>
                                            <fo:instream-foreign-object>
                                                <svg:svg width="750" height="609"
                                                         xmlns:svg="http://www.w3.org/2000/svg">
                                                    <svg:g transform="rotate(-20)">
                                                        <svg:text x="-5" y="300"
                                                                  xsl:use-attribute-sets="watermark-text">

                                                            <xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/watermarkText"/>
                                                        </svg:text>
                                                    </svg:g>
                                                </svg:svg>
                                            </fo:instream-foreign-object>
                                        </fo:block>
                                    </fo:block-container>
                                </xsl:if>
                                <fo:block xsl:use-attribute-sets="formTitle">Form 1098-T</fo:block>
                                <fo:block xsl:use-attribute-sets="line-break">&#160;</fo:block>
                                <fo:table xsl:use-attribute-sets="tableFirstRow">
                                    <fo:table-body>
                                        <fo:table-row>
                                            <fo:table-cell
                                                    number-rows-spanned="2" xsl:use-attribute-sets="cellBorder cell1">
                                                <fo:block xsl:use-attribute-sets="cell1label">
                                                    <fo:block>
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/filerInfo/filerInfoLabel"/>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/name1!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/name1"/>&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/name2!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/name2"/>
                                                            </fo:inline>
                                                        </xsl:if>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/filerInfo/address1"/>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/city!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/city"/>&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/state!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/state"/>&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                        <fo:inline>
                                                            <xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/filerInfo/zipCode"/>
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/nationCode!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/nationCode"/>&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/phoneArea!=''">
                                                            <fo:inline>
                                                                (<xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/filerInfo/phoneArea"/>)&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/phoneNumber!=''">
                                                            <fo:inline>
                                                                <xsl:value-of
                                                                        select="studentAccountTaxNotification/config/formSection/filerInfo/phoneNumber"/>&#160;
                                                            </fo:inline>
                                                        </xsl:if>
                                                        <xsl:if test="studentAccountTaxNotification/config/formSection/filerInfo/phoneExt!=''">
                                                            <fo:inline>
                                                                Ext.&#160;<xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/filerInfo/phoneExt"/>
                                                            </fo:inline>
                                                        </xsl:if>
                                                    </fo:block>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder cell2_1">
                                                <fo:block xsl:use-attribute-sets="cell2_1label">
                                                    <fo:block>
                                                        <fo:inline xsl:use-attribute-sets="boxNumber">1</fo:inline>
                                                        <fo:inline>
                                                            &#160;<xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/formInfo/box1/label"/>
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        $<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box1/value"/>
                                                    </fo:block>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder cell3">
                                                <fo:block xsl:use-attribute-sets="form-year">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/year"/>
                                                </fo:block>
                                            </fo:table-cell>
                                            <fo:table-cell number-rows-spanned="2" xsl:use-attribute-sets="cell4">
                                                <fo:block xsl:use-attribute-sets="cell4Label">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/formName"/>
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                        <fo:table-row>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder cell2_2">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">2</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box2/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    $<xsl:value-of
                                                        select="studentAccountTaxNotification/config/formSection/formInfo/box2/value"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cell3_1">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="form-1098-text-style">
                                                        Form&#160;
                                                    </fo:inline>
                                                    <fo:inline xsl:use-attribute-sets="form-1098-t">1098-T</fo:inline>
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-body>
                                </fo:table>
                                <fo:table xsl:use-attribute-sets="tableSecondRow">
                                    <fo:table-body>
                                        <fo:table-row>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t2cell1">
                                                <fo:block>
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/filerInfo/filerEmployerLabel"/>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/filerInfo/filerEmployeeId"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t2cell2">
                                                <fo:block>
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/studentTINLabel"/>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/studentTIN"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t2cell3">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">3</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box3/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="rectangle box3Rectangle">
                                                    <xsl:choose>
                                                        <xsl:when
                                                                test="contains(studentAccountTaxNotification/config/formSection/formInfo/box3/value,'true')">
                                                            &#x2611;
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            &#x2610;
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="borderTop t2cell4">
                                                <fo:block></fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-body>
                                </fo:table>
                                <fo:table xsl:use-attribute-sets="tableFinalRow">
                                    <fo:table-body>
                                        <fo:table-row>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r1cell1"
                                                           number-columns-spanned="2" number-rows-spanned="2">
                                                <fo:block xsl:use-attribute-sets="t3r1cell1label">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/studentName"/>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    <xsl:if test="studentAccountTaxNotification/config/formSection/studentInfo/firstName!=''">
                                                        <fo:inline>
                                                            <xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/studentInfo/firstName"/>&#160;
                                                        </fo:inline>
                                                    </xsl:if>
                                                    <xsl:if test="studentAccountTaxNotification/config/formSection/studentInfo/middleName!=''">
                                                        <fo:inline>
                                                            <xsl:value-of
                                                                    select="studentAccountTaxNotification/config/formSection/studentInfo/middleName"/>&#160;
                                                        </fo:inline>
                                                    </xsl:if>
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/lastName"/>
                                                </fo:block>

                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/address1"/>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/address2"/>&#160;
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/address3"/>
                                                    </fo:block>

                                                    <fo:block xsl:use-attribute-sets="cellValue">
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/city"/>;

                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/state"/>&#160;
                                                        <xsl:value-of
                                                                select="studentAccountTaxNotification/config/formSection/studentInfo/zipCode"/>

                                                    </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r1cell2">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">4</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box4/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    $<xsl:value-of
                                                        select="studentAccountTaxNotification/config/formSection/formInfo/box4/value"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r1cell3">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">5</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box5/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    $<xsl:value-of
                                                        select="studentAccountTaxNotification/config/formSection/formInfo/box5/value"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="borderBottom t3r1cell4"
                                                           number-rows-spanned="3">
                                                <fo:block xsl:use-attribute-sets="disclaimerNote">Note:</fo:block>
                                                <fo:block>
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/disclaimer/message"/>

                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                        <fo:table-row>

                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r2cell2"
                                                           >
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">6</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box6/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    $<xsl:value-of
                                                        select="studentAccountTaxNotification/config/formSection/formInfo/box6/value"/>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r2cell3"
                                                           >
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">7</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box7/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="rectangle box7Rectangle">
                                                    <xsl:choose>
                                                        <xsl:when
                                                                test="contains(studentAccountTaxNotification/config/formSection/formInfo/box7/value,'true')">
                                                            &#x2611;
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            &#x2610;
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </fo:block>

                                            </fo:table-cell>
                                        </fo:table-row>
<!--
                                        <fo:table-row>

                                            <fo:table-cell xsl:use-attribute-sets="borderTop t3r3cell1"
                                                           >
                                                <fo:block></fo:block>
                                            </fo:table-cell>

                                        </fo:table-row>
-->
                                        <fo:table-row>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r4cell1">
                                                <fo:block>
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/serviceProviderLabel"/>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    <xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/studentInfo/serviceProvider"/>
                                                </fo:block>
                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r4cell2">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">8</fo:inline>
                                                    <fo:inline xsl:use-attribute-sets="t3r4cell2label">
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box8/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="rectangle box8Rectangle">
                                                    <xsl:choose>
                                                        <xsl:when
                                                                test="contains(studentAccountTaxNotification/config/formSection/formInfo/box8/value,'true')">
                                                            &#x2611;
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            &#x2610;
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r4cell3">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">9</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box9/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block
                                                        xsl:use-attribute-sets="t3r4cell3label rectangle box9Rectangle">
                                                    <xsl:choose>
                                                        <xsl:when
                                                                test="contains(studentAccountTaxNotification/config/formSection/formInfo/box9/value,'true')">
                                                            &#x2611;
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            &#x2610;
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </fo:block>

                                            </fo:table-cell>
                                            <fo:table-cell xsl:use-attribute-sets="cellBorder t3r4cell4">
                                                <fo:block>
                                                    <fo:inline xsl:use-attribute-sets="boxNumber">10</fo:inline>
                                                    <fo:inline>
                                                        &#160;<xsl:value-of
                                                            select="studentAccountTaxNotification/config/formSection/formInfo/box10/label"/>
                                                    </fo:inline>
                                                </fo:block>
                                                <fo:block xsl:use-attribute-sets="cellValue">
                                                    $<xsl:value-of
                                                        select="studentAccountTaxNotification/config/formSection/formInfo/box10/value"/>
                                                </fo:block>
                                            </fo:table-cell>
                                        </fo:table-row>
                                    </fo:table-body>
                                </fo:table>
                            </fo:block-container>
                            <xsl:if test="studentAccountTaxNotification/detail='true'">
                                <fo:block-container xsl:use-attribute-sets="pageInfo">
                                    <fo:block xsl:use-attribute-sets="pageInfoText">
                                        <fo:external-graphic
                                                xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                        <fo:bidi-override
                                                unicode-bidi="embed"
                                                direction="ltr">
                                            <xsl:value-of
                                                    select="studentAccountTaxNotification/config/supplementalInformation"/>
                                        </fo:bidi-override>
                                    </fo:block>
                                </fo:block-container>
                                <fo:block-container xsl:use-attribute-sets="pageTitle">
                                    <fo:block>
                                        <xsl:value-of select="exsl:node-set($labels)/supplementalInformationTitle"/>
                                    </fo:block>
                                </fo:block-container>
                                <fo:block-container>
                                    <fo:block
                                            xsl:use-attribute-sets="margin">
                                        <fo:table>
                                            <xsl:choose>
                                                <xsl:when
                                                        test="studentAccountTaxNotification/config/formSection/formInfo/studentNotification='S'">
                                                    <fo:table-column
                                                            column-width="14%"/>
                                                    <fo:table-column column-width="3%"/>
                                                    <fo:table-column column-width="3%"/>
                                                    <fo:table-column
                                                            column-width="84%"/>

                                                    <fo:table-body>
                                                        <fo:table-row
                                                                xsl:use-attribute-sets="zero-margin hardCopySent">
                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin hardCopySentHeader">
                                                                    <xsl:value-of
                                                                            select="exsl:node-set($labels)/hardCopyMailed"/>
                                                                </fo:block>
                                                            </fo:table-cell>

                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin">
                                                                    <xsl:value-of
                                                                            select="studentAccountTaxNotification/config/formSection/formInfo/hardCopyMailed"/>
                                                                </fo:block>
                                                            </fo:table-cell>

                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin">
                                                                    on
                                                                </fo:block>
                                                            </fo:table-cell>
                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin ">
                                                                    <xsl:value-of
                                                                            select="studentAccountTaxNotification/config/formSection/formInfo/studentNotificationDate"/>
                                                                </fo:block>
                                                            </fo:table-cell>

                                                        </fo:table-row>
                                                    </fo:table-body>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <fo:table-column
                                                            column-width="14%"/>
                                                    <fo:table-column column-width="86%"/>

                                                    <fo:table-body>
                                                        <fo:table-row
                                                                xsl:use-attribute-sets="zero-margin hardCopySent">
                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin hardCopySentHeader">
                                                                    <xsl:value-of
                                                                            select="exsl:node-set($labels)/hardCopyMailed"/>
                                                                </fo:block>
                                                            </fo:table-cell>

                                                            <fo:table-cell
                                                                    xsl:use-attribute-sets="zero-margin">
                                                                <fo:block
                                                                        xsl:use-attribute-sets="zero-margin">
                                                                    <xsl:value-of
                                                                            select="studentAccountTaxNotification/config/formSection/formInfo/hardCopyMailed"/>
                                                                </fo:block>
                                                            </fo:table-cell>
                                                        </fo:table-row>
                                                    </fo:table-body>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </fo:table>
                                    </fo:block>
                                </fo:block-container>
                                <xsl:for-each select="studentAccountTaxNotification/supplementalReports">
                                    <fo:table>
                                        <fo:table-column column-width="75%"/>
                                        <fo:table-column column-width="25%"/>
                                        <fo:table-body>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell>
                                                    <fo:block xsl:use-attribute-sets="accordion-description">
                                                        <xsl:value-of select="reportDescription"/>
                                                    </fo:block>
                                                </fo:table-cell>
                                                <fo:table-cell>
                                                    <fo:block
                                                            xsl:use-attribute-sets="accordion-amountDisplay numeric-align">
                                                        <xsl:value-of select="totalDisplay"/>
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                    <fo:table>
                                        <fo:table-body>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:table xsl:use-attribute-sets="table">
                                                        <fo:table-header>
                                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showEligibleYear='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/eligibleYear"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showChargesBilledCap='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/chargesBilledCap"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showPaymentsReceived='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/paymentsReceived"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showSchOrGrants='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/schOrGrants"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showTerm='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                        <fo:block
                                                                                xsl:use-attribute-sets="table-column-header">
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/term"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showDescription='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-wide-column">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/description"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showAmount='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/amount"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showReportedDate='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/reportedDate"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicator='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/futureIndicator"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorPR='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/futureIndicatorPR"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorSG='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/futureIndicatorSG"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showProRata='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/supplementalGridProRata"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                            </fo:table-row>
                                                        </fo:table-header>
                                                        <fo:table-body>
                                                            <xsl:for-each
                                                                    select="result">
                                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showEligibleYear='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="eligibleYear"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showChargesBilledCap='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="chargesBilledCap"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showPaymentsReceived='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="paymentsReceived"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showSchOrGrants='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="schOrGrants"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showTerm='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="term"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showDescription='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="description"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showAmount='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin ">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="amountDisplay"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showReportedDate='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="reportedDate"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicator='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="futureIndicator"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorPR='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="futureIndicatorPR"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorSG='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="futureIndicatorSG"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showProRata='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="proRata"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                </fo:table-row>
                                                            </xsl:for-each>
                                                            <!--Total-->
                                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showEligibleYear='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin totalRow">
                                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRowLabelForSR='true'">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/totalLabelForSR"/>
                                                                        </fo:block>
                                                                        </xsl:if>
                                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRowLabelForSR='false'">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                            </fo:block>
                                                                         </xsl:if>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showChargesBilledCap='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showPaymentsReceived='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showSchOrGrants='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showTerm='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showDescription='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showAmount='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin totalRow ">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                            <xsl:value-of
                                                                                    select="totals/totalamountDisplay"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showReportedDate='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">

                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicator='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">

                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorPR='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">

                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showFutureIndicatorSG='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">

                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/supplementalGridConfig/showProRata='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                        <fo:block xsl:use-attribute-sets="margin">
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                            </fo:table-row>

                                                        </fo:table-body>
                                                    </fo:table>
                                                </fo:table-cell>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                </xsl:for-each>
                                <xsl:for-each select="studentAccountTaxNotification/detailReports">
                                    <xsl:if test="showGrid='true'">
                                        <fo:table>
                                            <fo:table-body>
                                                <fo:table-row>
                                                    <fo:table-cell>
                                                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <fo:table-row>
                                                    <fo:table-cell>
                                                        <fo:block xsl:use-attribute-sets="accordion-description">
                                                            <xsl:value-of select="reportDescription"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                                <fo:table-row>
                                                    <fo:table-cell>
                                                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                            </fo:table-body>
                                        </fo:table>
                                        <fo:table>
                                            <fo:table-body>
                                                <fo:table-row>
                                                    <fo:table-cell number-columns-spanned="2">
                                                        <fo:table xsl:use-attribute-sets="table">
                                                            <fo:table-header>
                                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTerm='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/detailGridTerm"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDetailCode='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/detailGridDetailCode"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTransaction='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow table-column-header">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/detailGridDescription"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showSumOfTaxAmount='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                            <fo:block
                                                                                    xsl:use-attribute-sets="margin numeric-align">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/detailGridSumOfTaxAmount"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showProRated='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                            <fo:block
                                                                                    xsl:use-attribute-sets="proRataMargin">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/detailGridProRata"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>

                                                                </fo:table-row>
                                                            </fo:table-header>
                                                            <fo:table-body>
                                                                <xsl:for-each
                                                                        select="result">
                                                                    <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                        <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTerm='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="descriptionOfTerm"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDetailCode='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="detailCode"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTransaction='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="descriptionOfTransaction"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showSumOfTaxAmount='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="grid-data tableRow supplemental-data margin ">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="margin numeric-align">
                                                                                    <xsl:value-of
                                                                                            select="sumOfTaxAmountDisplay"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>
                                                                        <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showProRated='true'">
                                                                            <fo:table-cell
                                                                                    xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                                <fo:block
                                                                                        xsl:use-attribute-sets="proRataMargin">
                                                                                    <xsl:value-of
                                                                                            select="proRated"/>
                                                                                </fo:block>
                                                                            </fo:table-cell>
                                                                        </xsl:if>

                                                                    </fo:table-row>
                                                                </xsl:for-each>
                                                                <!--Total-->
                                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTerm='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data  totalRow">
                                                                            <xsl:if test="exsl:node-set($config)/custom/showTotalRowLabel='true'">
                                                                                <fo:block xsl:use-attribute-sets="margin">
                                                                                    <xsl:value-of
                                                                                            select="exsl:node-set($labels)/totalLabel"/>
                                                                                </fo:block>
                                                                            </xsl:if>
                                                                            <xsl:if test="exsl:node-set($config)/custom/showTotalRowLabel='false'">
                                                                                <fo:block xsl:use-attribute-sets="margin">
                                                                                </fo:block>
                                                                            </xsl:if>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDetailCode='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data ">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showDescriptionOfTransaction='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data ">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showSumOfTaxAmount='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data   totalRow">
                                                                            <fo:block
                                                                                    xsl:use-attribute-sets="margin numeric-align">
                                                                                <xsl:value-of
                                                                                        select="totals/totalsumOfTaxAmountDisplay"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/taxDetailGridConfig/showProRated='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>

                                                                </fo:table-row>

                                                            </fo:table-body>
                                                        </fo:table>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                            </fo:table-body>
                                        </fo:table>
                                    </xsl:if>
                                </xsl:for-each>
                                <xsl:for-each select="studentAccountTaxNotification/recnReports">
                                    <fo:table>
                                        <fo:table-column column-width="75%"/>
                                        <fo:table-column column-width="25%"/>
                                        <fo:table-body>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell>
                                                    <fo:block xsl:use-attribute-sets="accordion-description">
                                                        <xsl:value-of select="reportDescription"/>
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                    <fo:table>
                                        <fo:table-body>
                                            <fo:table-row>
                                                <fo:table-cell number-columns-spanned="2">
                                                    <fo:table xsl:use-attribute-sets="table">
                                                        <fo:table-header>
                                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showEligibleYear='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header ">
                                                                        <fo:block>
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/eligibleYear"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showPreCarryFwd='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/preCarryFwd"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showChargesBilled='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/chargesBilled"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCurDecPrevCharges='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/curDecPrevCharges"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showBox4Adj='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/box4Adj"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCapLimit='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column  ">
                                                                        <fo:block
                                                                                xsl:use-attribute-sets="table-column-header">
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/capLimit"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCurrPayRecvd='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-wide-column">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/currPayRecvd"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showBox1Pay='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/box1Pay"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                                <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showEndCarryFwd='true'">
                                                                    <fo:table-cell
                                                                            xsl:use-attribute-sets="grid-data tableRow table-column-header supplemental-amount-column ">
                                                                        <fo:block
                                                                        >
                                                                            <xsl:value-of
                                                                                    select="exsl:node-set($labels)/endCarryFwd"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>
                                                                </xsl:if>
                                                            </fo:table-row>
                                                        </fo:table-header>
                                                        <fo:table-body>
                                                            <xsl:for-each
                                                                    select="result">
                                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showEligibleYear='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="eligibleYear"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showPreCarryFwd='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block
                                                                            >
                                                                                <xsl:value-of
                                                                                        select="preCarryFwd"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showChargesBilled='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="chargesBilled"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCurDecPrevCharges='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="curDecPrevCharges"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showBox4Adj='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="box4Adj"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCapLimit='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="capLimit"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showCurrPayRecvd='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="currPayRecvd"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showBox1Pay='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin ">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="box1Pay"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                    <xsl:if test="exsl:node-set($config)/custom/recnGridConfig/showEndCarryFwd='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="grid-data tableRow supplemental-data margin">
                                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                                <xsl:value-of
                                                                                        select="endCarryFwd"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>
                                                                </fo:table-row>
                                                            </xsl:for-each>
                                                        </fo:table-body>
                                                    </fo:table>
                                                </fo:table-cell>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                </xsl:for-each>

                            </xsl:if>
                        </xsl:if>
                        <fo:block xsl:use-attribute-sets="line-break">&#160;</fo:block>
                    </fo:block-container>
                    <fo:block-container xsl:use-attribute-sets="container disclaimer">
                        <xsl:if test="exsl:node-set($config)/custom/displayDisclaimerInfoOnPrintPreview = 'true'">
                            <fo:block xsl:use-attribute-sets="disclaimerHeader">
                                <xsl:value-of select="exsl:node-set($labels)/disclaimer"/>
                            </fo:block>
                            <fo:block xsl:use-attribute-sets="disclaimerMessage">
                                <xsl:value-of select="studentAccountTaxNotification/disclaimerMessage"/>
                            </fo:block>
                        </xsl:if>
                        <fo:block></fo:block>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
