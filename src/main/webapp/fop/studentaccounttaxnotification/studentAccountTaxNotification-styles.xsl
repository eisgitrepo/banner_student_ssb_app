<?xml version="1.0" encoding="UTF-8" ?>
<!-- Copyright 2019 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl">

    <!--accordion- description-->
    <xsl:attribute-set name="accordion-description">
        <xsl:attribute name="height">30px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">24px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.25</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <!--accordion- Amount display-->
    <xsl:attribute-set name="accordion-amountDisplay">
        <xsl:attribute name="height">24px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">21px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.14</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="accordion">
        <xsl:attribute name="padding-top">24px</xsl:attribute>
        <xsl:attribute name="padding-bottom">24px</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="divider">
        <xsl:attribute name="border-top">1pt solid #5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="totalRow">
        <xsl:attribute name="height">20px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="calculation-description">
        <xsl:attribute name="height">24px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="calculation-amount">
        <xsl:attribute name="height">24px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="calculation-asOf">
        <xsl:attribute name="font-size">14px;</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="pageInfo">
        <xsl:attribute name="border-top">2pt solid #51abff</xsl:attribute>
        <xsl:attribute name="background-color">#eff7ff</xsl:attribute>
        <xsl:attribute name="margin-left">16px</xsl:attribute>
        <xsl:attribute name="margin-right">16px</xsl:attribute>
        <xsl:attribute name="margin-top">20px</xsl:attribute>
    </xsl:attribute-set>


    <!-- Overall page layout. -->
    <xsl:attribute-set name="page">
        <xsl:attribute name="page-height">1684pt</xsl:attribute>
        <xsl:attribute name="page-width">1100pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="preserve-line-and-space">
        <xsl:attribute name="white-space-collapse">false</xsl:attribute>
        <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
        <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    </xsl:attribute-set>

    <!-- Header region for the taxNotification logo and address when printed at top of the page. -->
    <xsl:attribute-set name="header-region">
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>

    <!-- Footer region for the taxNotification logo and address when printed at bottom of the page. -->
    <xsl:attribute-set name="footer-region">
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>
    <!-- Body region for main content of the page. -->
    <xsl:attribute-set name="body-region">
        <xsl:attribute name="margin-top">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'TOP'">
                    100pt
                </xsl:when>
                <xsl:otherwise>
                    0in
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="margin-bottom">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'BOTTOM'">
                    .7in
                </xsl:when>
                <xsl:otherwise>
                    0in
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Container content within a region - font styles, writing direction -->
    <xsl:attribute-set name="container">
        <xsl:attribute name="writing-mode">
            <xsl:value-of select="$base-writing-mode"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">
            <xsl:value-of select="$text-align-left"/>
        </xsl:attribute>
        <xsl:attribute name="margin-left">16pt</xsl:attribute>
        <xsl:attribute name="margin-right">16pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Section content within a container -->
    <xsl:attribute-set name="section">
        <xsl:attribute name="margin-top">4mm</xsl:attribute>
        <xsl:attribute name="keep-together.within-column">always</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="table">
        <xsl:attribute name="table-layout">fixed</xsl:attribute>
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
    </xsl:attribute-set>


    <!-- Table styles. -->
    <xsl:attribute-set name="tableRow">
        <xsl:attribute name="border-bottom">1pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="height">48pt</xsl:attribute>
    </xsl:attribute-set>


    <!-- Table caption styles. -->
    <xsl:attribute-set name="table-caption">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="margin-bottom">2pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Line break. -->
    <xsl:attribute-set name="line-break">
        <xsl:attribute name="white-space-collapse">false</xsl:attribute>
        <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
        <xsl:attribute name="font-size">15pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Margin. -->
    <xsl:attribute-set name="margin">
        <xsl:attribute name="margin">4pt, 0pt, 4pt, 0pt</xsl:attribute>
    </xsl:attribute-set>


    <!-- Margin. -->
    <xsl:attribute-set name="numeric-align">
        <xsl:attribute name="text-align">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'ltr'">
                    right
                </xsl:when>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'rtl'">
                    left
                </xsl:when>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="cellBorder">
        <xsl:attribute name="border">1pt solid black</xsl:attribute>
        <xsl:attribute name="margin">0pt 0pt 0pt 8pt</xsl:attribute>
        <xsl:attribute name="padding">0pt 0pt 0pt 0pt</xsl:attribute>
        <xsl:attribute name="font-family">OpenSans</xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="borderTop">
        <xsl:attribute name="border-top">1pt solid black</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="borderBottom">
        <xsl:attribute name="border-bottom">1pt solid black</xsl:attribute>
    </xsl:attribute-set>


    <!-- Table cell styles  -->
    <xsl:attribute-set name="table-cell">

        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="text-align">
            left
        </xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table column header cell styling -->
    <xsl:attribute-set name="table-column-header" use-attribute-sets="table-cell">
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
        <xsl:attribute name="padding">0pt,0pt,0pt,0pt</xsl:attribute>
        <xsl:attribute name="margin">0pt,0pt,0pt,0pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table row header cell styling -->
    <xsl:attribute-set name="table-row-header" use-attribute-sets="table-cell">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table data cell with row header style -->
    <xsl:attribute-set name="table-data-with-row-header" use-attribute-sets="table-cell">
        <xsl:attribute name="border">2pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>


    <xsl:attribute-set name="no-data-msg">
        <xsl:attribute name="margin">0pt</xsl:attribute>
        <xsl:attribute name="padding">3pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="border">1pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>

    <!--
    =====================================================================================================
         The following attribute definitions are for section specific elements.
         Many of the attributes inherit common styles using use-attribute-sets which produces a similar
         effect to cascading stylesheets.
    =====================================================================================================
     -->
    <!-- taxNotification section styles -->
    <xsl:attribute-set name="taxNotification-table" use-attribute-sets="table">
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>

        <xsl:attribute name="writing-mode">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoLeftRight = 'LEFT'">
                    lr
                </xsl:when>
                <xsl:otherwise>
                    rl
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="noRTL" use-attribute-sets="table">
        <xsl:attribute name="writing-mode">
            lr
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxNotification-logo-column">
        <xsl:attribute name="column-width">17%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxNotification-contact-info-column">
        <xsl:attribute name="column-width">10%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="label">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxNotification-data" use-attribute-sets="table-cell">
        <!--<xsl:attribute name="font-size">80%</xsl:attribute>-->
    </xsl:attribute-set>
    <xsl:attribute-set name="taxNotification-cla-data" use-attribute-sets="table-cell">
        <xsl:attribute name="font-size">80%</xsl:attribute>
        <xsl:attribute name="background-color">rgb(242, 242, 242)</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxNotification-logo">
        <xsl:attribute name="src">
            <xsl:value-of
                    select="concat('url(', /pdfModel/logoFile/logoFileLocation,')')"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="width">190pt</xsl:attribute>
        <xsl:attribute name="height">38.9pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="taxNotification-form-table">
        <xsl:attribute name="border">1pt solid black</xsl:attribute>

        <xsl:attribute name="writing-mode">
            lr
        </xsl:attribute>
    </xsl:attribute-set>

    <!--info circle-->
    <xsl:attribute-set name="accountSummary-info-circle-icon">
        <xsl:attribute name="src">
            <xsl:value-of
                    select="concat('url(', /pdfModel/infoCircle,')')"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="width">24pt</xsl:attribute>
        <xsl:attribute name="height">19pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="pageTitle">
        <xsl:attribute name="height">32</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">26pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.23</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="margin-top">30pt</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>


    <!-- pageInfoText field. -->
    <xsl:attribute-set name="pageInfoText">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.38</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#2874bb</xsl:attribute>
        <xsl:attribute name="padding-top">24pt</xsl:attribute>
        <xsl:attribute name="padding-bottom">24pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- pageInfo. -->
    <xsl:attribute-set name="pageInfo">
        <xsl:attribute name="border-top">2pt solid #51abff</xsl:attribute>
        <xsl:attribute name="background-color">#eff7ff</xsl:attribute>
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
        <xsl:attribute name="margin-top">20pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- userInfo field. -->
    <xsl:attribute-set name="userInfo">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#000000</xsl:attribute>
        <xsl:attribute name="padding-top">20pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- userName field. -->
    <xsl:attribute-set name="userName">
        <xsl:attribute name="font-weight">normal</xsl:attribute>
    </xsl:attribute-set>

    <!-- clientDate field. -->
    <xsl:attribute-set name="clientDate">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#000000</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Header-->
    <xsl:attribute-set name="disclaimerHeader">
        <xsl:attribute name="height">24pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Message-->
    <xsl:attribute-set name="disclaimerMessage">
        <xsl:attribute name="height">24pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.5</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Message-->
    <xsl:attribute-set name="disclaimer">
        <xsl:attribute name="margin-top">30pt</xsl:attribute>
    </xsl:attribute-set>


    <!--pageHeader-->
    <xsl:attribute-set name="pageHeader">
        <xsl:attribute name="border-bottom">1pt solid #5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="zero-margin">
        <xsl:attribute name="margin">0pt,0pt,0pt,0pt</xsl:attribute>
        <xsl:attribute name="padding">0pt, 0pt, 0pt, 0pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tableFirstRow">
        <xsl:attribute name="width">879pt</xsl:attribute>
        <xsl:attribute name="height">148pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell1">
        <xsl:attribute name="width">380pt</xsl:attribute>
        <xsl:attribute name="height">84pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell1label">
        <xsl:attribute name="width">368pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell2_1">
        <xsl:attribute name="width">161pt</xsl:attribute>
        <xsl:attribute name="height">84pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell2_1label">
        <xsl:attribute name="width">125pt</xsl:attribute>
        <xsl:attribute name="height">51pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell2_2">
        <xsl:attribute name="width">161pt</xsl:attribute>
        <xsl:attribute name="height">64pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell3">
        <xsl:attribute name="width">150pt</xsl:attribute>
        <xsl:attribute name="height">84pt</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="border-bottom">0pt</xsl:attribute>
        <xsl:attribute name="display-align">after</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="form-year">
        <xsl:attribute name="font-size">36px</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell3_1">
        <xsl:attribute name="width">150pt</xsl:attribute>
        <xsl:attribute name="height">64pt</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="border-right">1pt solid black</xsl:attribute>
        <xsl:attribute name="border-top">0pt</xsl:attribute>
        <xsl:attribute name="display-align">after</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="form-1098-t">
        <xsl:attribute name="font-size">
            18px
        </xsl:attribute>
        <xsl:attribute name="font-weight">
            bold
        </xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="form-1098-text-style">
        <xsl:attribute name="font-size">
            14px
        </xsl:attribute>
        <xsl:attribute name="font-weight">
            normal
        </xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell4">
        <xsl:attribute name="width">188pt</xsl:attribute>
        <xsl:attribute name="height">84pt</xsl:attribute>
        <xsl:attribute name="text-align">right</xsl:attribute>
        <xsl:attribute name="display-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cell4Label">
        <xsl:attribute name="width">96pt</xsl:attribute>
        <xsl:attribute name="height">48pt</xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="margin-left">10</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tableSecondRow">
        <xsl:attribute name="width">879pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell1">
        <xsl:attribute name="width">217pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell1Label">
        <xsl:attribute name="width">193pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell2">
        <xsl:attribute name="width">163pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell2label">
        <xsl:attribute name="min-width">100pt</xsl:attribute>
        <xsl:attribute name="max-width">120pt</xsl:attribute>
        <xsl:attribute name="min-height">17pt</xsl:attribute>
        <xsl:attribute name="max-height">25pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell3">
        <xsl:attribute name="width">369pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell3label">
        <xsl:attribute name="width">339pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t2cell4">
        <xsl:attribute name="width">130pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="tableFinalRow">
        <xsl:attribute name="width">879pt</xsl:attribute>
        <xsl:attribute name="height">264pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell1">
        <xsl:attribute name="width">356pt</xsl:attribute>
        <xsl:attribute name="height">98pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell1label">
        <xsl:attribute name="width">356pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell2">
        <xsl:attribute name="width">161pt</xsl:attribute>
        <xsl:attribute name="height">73pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell2label">
        <xsl:attribute name="width">125pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell3">
        <xsl:attribute name="width">208pt</xsl:attribute>
        <xsl:attribute name="height">73pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell3label">
        <xsl:attribute name="width">152pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell4">
        <xsl:attribute name="width">130pt</xsl:attribute>
        <xsl:attribute name="height">73pt</xsl:attribute>
        <xsl:attribute name="text-align">right</xsl:attribute>
        <xsl:attribute name="display-align">before</xsl:attribute>
        <xsl:attribute name="margin-left">30</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r1cell4label">
        <xsl:attribute name="width">105pt</xsl:attribute>
        <xsl:attribute name="height">133pt</xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell1">
        <xsl:attribute name="width">231pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell1label">
        <xsl:attribute name="width">188pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell2">
        <xsl:attribute name="width">161pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell2label">
        <xsl:attribute name="width">125pt</xsl:attribute>
        <xsl:attribute name="height">51pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell3">
        <xsl:attribute name="width">208pt</xsl:attribute>
        <xsl:attribute name="height">56pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r2cell3label">
        <xsl:attribute name="width">158pt</xsl:attribute>
        <xsl:attribute name="height">85pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r3cell1">
        <xsl:attribute name="width">0pt</xsl:attribute>
        <xsl:attribute name="height">0pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r3cell1label">
        <xsl:attribute name="width">356pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell1">
        <xsl:attribute name="width">220pt</xsl:attribute>
        <xsl:attribute name="height">60pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell1label">
        <xsl:attribute name="width">205pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell2">
        <xsl:attribute name="width">160pt</xsl:attribute>
        <xsl:attribute name="height">60pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell2label">
        <xsl:attribute name="width">120pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell3">
        <xsl:attribute name="width">161pt</xsl:attribute>
        <xsl:attribute name="height">60pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell3label">
        <xsl:attribute name="width">122pt</xsl:attribute>
        <xsl:attribute name="height">34pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell4">
        <xsl:attribute name="width">208pt</xsl:attribute>
        <xsl:attribute name="height">60pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="t3r4cell4label">
        <xsl:attribute name="width">152pt</xsl:attribute>
        <xsl:attribute name="height">17pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="watermark-text">
        <xsl:attribute name="opacity">0.1</xsl:attribute>
        <xsl:attribute name="font-family">OpenSans</xsl:attribute>
        <xsl:attribute name="font-size">90px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="boxNumber">
        <xsl:attribute name="width">8</xsl:attribute>
        <xsl:attribute name="height">20px</xsl:attribute>
        <xsl:attribute name="font-family">OpenSans</xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="rectangle">
        <xsl:attribute name="width">16</xsl:attribute>
        <xsl:attribute name="height">16</xsl:attribute>
        <xsl:attribute name="font-size">
            16px
        </xsl:attribute>
        <xsl:attribute name="text-align">right</xsl:attribute>
        <xsl:attribute name="line-height">12px</xsl:attribute>
        <xsl:attribute name="margin-right">9px</xsl:attribute>
        <xsl:attribute name="background-color">#d9d9d9</xsl:attribute>
        <xsl:attribute name="font-family">freeSerif</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="box3Rectangle">
        <xsl:attribute name="margin-left">91.4%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="box7Rectangle">
        <xsl:attribute name="margin-left">85.1%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="box9Rectangle">
        <xsl:attribute name="margin-left">80.5%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="box8Rectangle">
        <xsl:attribute name="margin-left">83.5%</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="cellValue">
        <xsl:attribute name="height">20</xsl:attribute>
        <xsl:attribute name="font-family">OpenSans</xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="disclaimerNote">
        <xsl:attribute name="height">20</xsl:attribute>
        <xsl:attribute name="font-family">OpenSans</xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="formTitle">
        <xsl:attribute name="width">144</xsl:attribute>
        <xsl:attribute name="height">32</xsl:attribute>
        <xsl:attribute name="font-family">Nunito</xsl:attribute>
        <xsl:attribute name="font-size">24px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="hardCopySentHeader">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="height">22</xsl:attribute>
        <xsl:attribute name="font-size">16px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.38</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="hardCopySent">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="height">22</xsl:attribute>
        <xsl:attribute name="font-size">16px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.38</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="accordion-description">
        <xsl:attribute name="height">32px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">24px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>


    <!--accordion- Amount display-->
    <xsl:attribute-set name="accordion-amountDisplay">
        <xsl:attribute name="height">28px</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">21px</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="supplemental-wide-column">
        <xsl:attribute name="width">110px</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="supplemental-amount-column">
        <xsl:attribute name="width">100px</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="supplemental-data">
        <xsl:attribute name="height">20px</xsl:attribute>
        <xsl:attribute name="font-size">14px</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="padding">0pt, 0pt, 0pt, 0pt</xsl:attribute>
        <xsl:attribute name="margin">0pt, 0pt, 0pt, 0pt</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="grid-data" use-attribute-sets="table-cell">
        <xsl:attribute name="font-size">80%</xsl:attribute>
        <xsl:attribute name="padding">2pt, 1pt, 1pt, 1pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="proRataMargin">
        <xsl:attribute name="margin-left">16pt</xsl:attribute>
        <xsl:attribute name="margin-top">4pt</xsl:attribute>
    </xsl:attribute-set>
</xsl:stylesheet>
