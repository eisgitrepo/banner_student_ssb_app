<?xml version="1.0" encoding="UTF-8" ?>
<!-- Copyright 2018 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl">

    <!-- Overall page layout. -->
    <xsl:attribute-set name="page">
        <xsl:attribute name="page-height">1684pt</xsl:attribute>
        <xsl:attribute name="page-width">1100pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="preserve-line-and-space">
        <xsl:attribute name="white-space-collapse">false</xsl:attribute>
        <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
        <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    </xsl:attribute-set>

    <!-- Header region for the accountSummaryOverview logo and address when printed at top of the page. -->
    <xsl:attribute-set name="header-region">
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>

    <!-- Footer region for the accountSummaryOverview logo and address when printed at bottom of the page. -->
    <xsl:attribute-set name="footer-region">
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>
    <!-- Body region for main content of the page. -->
    <xsl:attribute-set name="body-region">
        <xsl:attribute name="margin-top">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'TOP'">
                    100pt
                </xsl:when>
                <xsl:otherwise>
                    0in
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="margin-bottom">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'BOTTOM'">
                    .7in
                </xsl:when>
                <xsl:otherwise>
                    0in
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Container content within a region - font styles, writing direction -->
    <xsl:attribute-set name="container">
        <xsl:attribute name="writing-mode">
            <xsl:value-of select="$base-writing-mode"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">
            <xsl:value-of select="$text-align-left"/>
        </xsl:attribute>
        <xsl:attribute name="margin-left">16pt</xsl:attribute>
        <xsl:attribute name="margin-right">16pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Section content within a container -->
    <xsl:attribute-set name="section">
        <xsl:attribute name="margin-top">4mm</xsl:attribute>
        <xsl:attribute name="keep-together.within-column">always</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="table">
        <xsl:attribute name="table-layout">fixed</xsl:attribute>
        <xsl:attribute name="border-collapse">collapse</xsl:attribute>
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="table-calculation">
        <xsl:attribute name="inline-progression-dimension">auto</xsl:attribute>
        <xsl:attribute name="table-layout">auto</xsl:attribute>
        <xsl:attribute name="border-top">0pt solid #5b5e65</xsl:attribute>
        <xsl:attribute name="border-bottom">0pt solid #5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="tableRow">
        <xsl:attribute name="border-bottom">1pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="height">38pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="totalRow">
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
        <xsl:attribute name="border-bottom">1pt solid #b2b3b7</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table caption styles. -->
    <xsl:attribute-set name="table-caption">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="margin-bottom">2pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Line break. -->
    <xsl:attribute-set name="line-break">
        <xsl:attribute name="white-space-collapse">false</xsl:attribute>
        <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
        <xsl:attribute name="font-size">15pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Margin. -->
    <xsl:attribute-set name="margin">
        <xsl:attribute name="margin">4pt, 0pt, 4pt, 0pt</xsl:attribute>
    </xsl:attribute-set>


    <!-- Margin. -->
    <xsl:attribute-set name="numeric-align">
        <xsl:attribute name="text-align">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'ltr'">
                    right
                </xsl:when>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'rtl'">
                    left
                </xsl:when>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>


    <!-- Table cell styles  -->
    <xsl:attribute-set name="table-cell">

        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="margin">0pt, 0pt, 0pt, 0pt</xsl:attribute>
        <xsl:attribute name="padding">3pt, 2pt, 2pt, 2pt</xsl:attribute>
        <xsl:attribute name="text-align">
            <xsl:value-of select="$text-align-left"/>
        </xsl:attribute>
        <xsl:attribute name="border-bottom">1pt solid #ffffff</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table column header cell styling -->
    <xsl:attribute-set name="table-column-header" use-attribute-sets="table-cell">
        <xsl:attribute name="background-color">#ffffff</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>

    </xsl:attribute-set>

    <!-- Table row header cell styling -->
    <xsl:attribute-set name="table-row-header" use-attribute-sets="table-cell">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table data cell with row header style -->
    <xsl:attribute-set name="table-data-with-row-header" use-attribute-sets="table-cell">
        <xsl:attribute name="border">2pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>

    <!-- Modifies the table cell style for numeric fields the need to decimal align. -->
    <xsl:attribute-set name="table-cell.fixed-length">
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="width">28pt</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-cell.fixed-decimal">
        <xsl:attribute name="text-align">right</xsl:attribute>
        <xsl:attribute name="width">28pt</xsl:attribute>
        <xsl:attribute name="height">20pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <!-- Modifies the table cell style for total cells. -->
    <xsl:attribute-set name="table-cell.total">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="no-data-msg">
        <xsl:attribute name="margin">0pt</xsl:attribute>
        <xsl:attribute name="padding">3pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="border">1pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>

    <!--
    =====================================================================================================
         The following attribute definitions are for section specific elements.
         Many of the attributes inherit common styles using use-attribute-sets which produces a similar
         effect to cascading stylesheets.
    =====================================================================================================
     -->
    <!-- accountSummaryOverview section styles -->
    <xsl:attribute-set name="accountSummaryOverview-table" use-attribute-sets="table">
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>

        <xsl:attribute name="writing-mode">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoLeftRight = 'LEFT'">
                    lr
                </xsl:when>
                <xsl:otherwise>
                    rl
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="accountSummaryOverview-logo-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="accountSummaryOverview-contact-info-column">
        <xsl:attribute name="column-width">10%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="label">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="accountSummaryOverview-data" use-attribute-sets="table-cell">
        <xsl:attribute name="padding">2pt, 1pt, 1pt, 1pt</xsl:attribute>
        <xsl:attribute name="height">18pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.43</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="accountSummaryOverview-cla-data" use-attribute-sets="table-cell">
        <xsl:attribute name="font-size">80%</xsl:attribute>
        <xsl:attribute name="padding">2pt, 1pt, 1pt, 1pt</xsl:attribute>
        <xsl:attribute name="background-color">rgb(242, 242, 242)</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="accountSummaryOverview-logo">
        <xsl:attribute name="src">
            <xsl:value-of
                    select="concat('url(', /pdfModel/logoFile/logoFileLocation,')')"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="width">190pt</xsl:attribute>
        <xsl:attribute name="height">38.9pt</xsl:attribute>
    </xsl:attribute-set>

    <!--info circle-->
    <xsl:attribute-set name="accountSummary-info-circle-icon">
        <xsl:attribute name="src">
            <xsl:value-of
                    select="concat('url(', /pdfModel/infoCircle,')')"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="width">24pt</xsl:attribute>
        <xsl:attribute name="height">19pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="pageTitle">
        <xsl:attribute name="height">32</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">26pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.23</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="margin-top">30pt</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>


    <!-- calculationInfoText. -->
    <xsl:attribute-set name="calculationInfoText">
        <xsl:attribute name="height">18pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">12pt</xsl:attribute>
        <xsl:attribute name="font-weight">18pt</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.5</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
        <xsl:attribute name="margin-top">4pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- calculation field. -->
    <xsl:attribute-set name="calculation">
        <xsl:attribute name="height">24pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
        <xsl:attribute name="margin-top">20pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- pageInfoText field. -->
    <xsl:attribute-set name="pageInfoText">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.38</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#2874bb</xsl:attribute>
        <xsl:attribute name="padding-top">24pt</xsl:attribute>
        <xsl:attribute name="padding-bottom">24pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- pageInfo. -->
    <xsl:attribute-set name="pageInfo">
        <xsl:attribute name="border-top">2pt solid #51abff</xsl:attribute>
        <xsl:attribute name="background-color">#eff7ff</xsl:attribute>
        <xsl:attribute name="margin-left">32pt</xsl:attribute>
        <xsl:attribute name="margin-right">32pt</xsl:attribute>
        <xsl:attribute name="margin-top">20pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- userInfo field. -->
    <xsl:attribute-set name="userInfo">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">600</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#000000</xsl:attribute>
        <xsl:attribute name="padding-top">20pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- userName field. -->
    <xsl:attribute-set name="userName">
        <xsl:attribute name="font-weight">normal</xsl:attribute>
    </xsl:attribute-set>

    <!-- clientDate field. -->
    <xsl:attribute-set name="clientDate">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">14pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">normal</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#000000</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Header-->
    <xsl:attribute-set name="disclaimerHeader">
        <xsl:attribute name="height">24pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-Nunito"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">18pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.33</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#151618</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Message-->
    <xsl:attribute-set name="disclaimerMessage">
        <xsl:attribute name="height">24pt</xsl:attribute>
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family-OpenSans"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">16pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="font-style">normal</xsl:attribute>
        <xsl:attribute name="font-stretch">normal</xsl:attribute>
        <xsl:attribute name="line-height">1.5</xsl:attribute>
        <xsl:attribute name="letter-spacing">normal</xsl:attribute>
        <xsl:attribute name="color">#5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <!--Disclaimer Message-->
    <xsl:attribute-set name="disclaimer">
        <xsl:attribute name="margin-top">30pt</xsl:attribute>
    </xsl:attribute-set>


    <!--pageHeader-->
    <xsl:attribute-set name="pageHeader">
        <xsl:attribute name="border-bottom">1pt solid #5b5e65</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="zero-margin">
        <xsl:attribute name="margin">0pt,0pt,0pt,0pt</xsl:attribute>
        <xsl:attribute name="padding">0pt, 0pt, 0pt, 0pt</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
