<?xml version="1.0" encoding="UTF-16"?>
<!-- Copyright 2018 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl fo">

    <xsl:include href="../common/common-config.xsl"/>
    <xsl:include href="../styles/accountSummary-styles.xsl"/>
    <xsl:include href="studentaccountinformation-styles-custom.xsl"/>
    <xsl:template match="pdfModel">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="accountSummaryOverview-page"
                                       xsl:use-attribute-sets="page">
                    <fo:region-body xsl:use-attribute-sets="body-region"/>
                    <fo:region-before xsl:use-attribute-sets="header-region"/>
                    <fo:region-after xsl:use-attribute-sets="footer-region"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="accountSummaryOverview-page">

                <fo:static-content flow-name="xsl-region-before">
                    <fo:block-container xsl:use-attribute-sets="container pageHeader">
                        <fo:block>
                            <fo:table>
                                <fo:table-column column-width="50%"/>
                                <fo:table-column column-width="50%"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        <fo:table-cell>
                                            <fo:block>
                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                        and $base-writing-mode = 'lr')
                                                        or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                        and $base-writing-mode = 'rl')">
                                                    <xsl:choose>
                                                        <xsl:when test="logoFile/svgFile = 'false'">
                                                            <fo:external-graphic
                                                                    xsl:use-attribute-sets="accountSummaryOverview-logo"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <fo:instream-foreign-object>
                                                                <svg:svg width="150" height="80"
                                                                         xmlns:svg="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                    <svg:g>
                                                                        <svg:image x="0" y="0" width="140px"
                                                                                   height="60px">
                                                                            <xsl:attribute name="xlink:href">
                                                                                <xsl:value-of
                                                                                        select="logoFile/logoFileLocation"/>
                                                                            </xsl:attribute>
                                                                        </svg:image>
                                                                    </svg:g>
                                                                </svg:svg>
                                                            </fo:instream-foreign-object>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <fo:block-container xsl:use-attribute-sets="container">

                                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT' and $base-writing-mode = 'lr')
                                                                                                             or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                                                                             and $base-writing-mode = 'rl') ">

                                                    <fo:block xsl:use-attribute-sets="userInfo numeric-align"
                                                              width="auto">
                                                        <fo:block>
                                                            <xsl:value-of select="studentAccountInformation/fullName"/>
                                                        </fo:block>
                                                        <br/>
                                                        <fo:block xsl:use-attribute-sets="userName">
                                                            <xsl:value-of select="studentAccountInformation/username"/>
                                                        </fo:block>
                                                    </fo:block>
                                                    <fo:block xsl:use-attribute-sets="clientDate numeric-align"
                                                              width="auto">
                                                        <xsl:value-of select="studentAccountInformation/clientDate"/>
                                                    </fo:block>
                                                </xsl:if>
                                            </fo:block-container>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>
                    </fo:block-container>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container xsl:use-attribute-sets="container accountSummaryOverview-table">
                        <xsl:if test="studentAccountInformation/displayTransactions='true'">
                            <fo:block-container xsl:use-attribute-sets="pageTitle">
                                <fo:block text-align='left'>
                                    <xsl:value-of select="exsl:node-set($labels)/transactionTitle"/>
                                </fo:block>
                            </fo:block-container>
                            <fo:block-container xsl:use-attribute-sets="pageInfo">
                                <fo:block text-align='left' xsl:use-attribute-sets="pageInfoText">
                                    <fo:external-graphic
                                            xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                    <fo:bidi-override
                                            unicode-bidi="embed"
                                            direction="ltr">
                                        <xsl:value-of
                                                select="studentAccountInformation/transactions/infoMsg"/>
                                    </fo:bidi-override>
                                </fo:block>
                            </fo:block-container>
                            <xsl:if test="studentAccountInformation/hasData='false'">
                                <fo:block-container xsl:use-attribute-sets="pageInfo">

                                    <fo:block
                                            xsl:use-attribute-sets="pageInfoText">
                                        &#160;&#160;<fo:external-graphic
                                            xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160;
                                        &#160;<xsl:value-of
                                            select="studentAccountInformation/noRecordsInfoText"/>
                                    </fo:block>

                                </fo:block-container>
                            </xsl:if>
                            <xsl:if test="exsl:node-set($config)/custom/showCurrentAmountDue='true' or exsl:node-set($config)/custom/showAccountBalance='true'">
                                <fo:table xsl:use-attribute-sets="calculation table-calculation">
                                    <fo:table-column column-width="50%"/>
                                    <fo:table-column column-width="50%"/>
                                    <fo:table-body>
                                        <xsl:if test="exsl:node-set($config)/custom/showCurrentAmountDue='true'">
                                            <fo:table-row>
                                                <fo:table-cell>
                                                    <fo:block>
                                                        <fo:table>
                                                            <xsl:choose>
                                                                <xsl:when
                                                                        test="exsl:node-set($config)/languageDirection = 'rtl'">
                                                                    <fo:table-column column-width="28%"/>
                                                                    <fo:table-column column-width="8%"/>
                                                                    <fo:table-column column-width="64%"/>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <fo:table-column column-width="39%"/>
                                                                    <fo:table-column column-width="10%"/>
                                                                    <fo:table-column column-width="51%"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>

                                                            <fo:table-body>
                                                                <fo:table-row xsl:use-attribute-sets="zero-margin">
                                                                    <fo:table-cell xsl:use-attribute-sets="zero-margin">
                                                                        <fo:block xsl:use-attribute-sets="zero-margin">
                                                                            <xsl:value-of
                                                                                    select=" exsl:node-set($labels)/currentAmountDue"/>
                                                                        </fo:block>
                                                                    </fo:table-cell>

                                                                    <xsl:if
                                                                            test="exsl:node-set($config)/custom/hasAsOfDate='true'">
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="zero-margin">
                                                                            <fo:block
                                                                                    xsl:use-attribute-sets="zero-margin">
                                                                                <xsl:value-of
                                                                                        select="exsl:node-set($labels)/asOf"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                        <fo:table-cell
                                                                                xsl:use-attribute-sets="zero-margin">
                                                                            <fo:block
                                                                                    xsl:use-attribute-sets="zero-margin">
                                                                                <xsl:value-of
                                                                                        select="studentAccountInformation/asOfDate"/>
                                                                            </fo:block>
                                                                        </fo:table-cell>
                                                                    </xsl:if>

                                                                </fo:table-row>
                                                            </fo:table-body>
                                                        </fo:table>
                                                    </fo:block>
                                                </fo:table-cell>
                                                <fo:table-cell
                                                        xsl:use-attribute-sets="numeric-align">
                                                    <fo:block>
                                                        <xsl:value-of
                                                                select="studentAccountInformation/currentAmountDueDisplay"/>
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                            <xsl:if test="exsl:node-set($config)/custom/showCurrentAmountInfo='true'">
                                                <fo:table-row>
                                                    <fo:table-cell number-columns-spanned="2">
                                                        <fo:block xsl:use-attribute-sets="calculationInfoText">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/currentAmountDueInfoText"/>
                                                        </fo:block>
                                                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </fo:table-row>
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="exsl:node-set($config)/custom/showAccountBalance='true'">
                                            <fo:table-row>
                                                <fo:table-cell>
                                                    <fo:block>
                                                        <xsl:value-of select="exsl:node-set($labels)/accountBalance"/>
                                                    </fo:block>
                                                    <xsl:if test="exsl:node-set($config)/custom/showAccountBalanceInfo='true'">
                                                        <fo:block xsl:use-attribute-sets="calculationInfoText">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/accountBalanceInfoText"/>
                                                        </fo:block>
                                                    </xsl:if>
                                                </fo:table-cell>
                                                <fo:table-cell xsl:use-attribute-sets="numeric-align">
                                                    <fo:block>
                                                        <xsl:value-of
                                                                select="studentAccountInformation/totalBalanceDisplay"/>
                                                    </fo:block>
                                                </fo:table-cell>
                                            </fo:table-row>
                                        </xsl:if>
                                    </fo:table-body>
                                </fo:table>
                            </xsl:if>
                            <fo:block xsl:use-attribute-sets="line-break">&#160;
                            </fo:block>
                            <xsl:if test="exsl:node-set($config)/custom/showCurrentAmountDue='true'">
                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                </fo:block>
                            </xsl:if>
                            <xsl:if test="studentAccountInformation/transactions/showGrid='true'">
                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                </fo:block>
                                <fo:block-container>
                                    <fo:table xsl:use-attribute-sets="table">
                                        <fo:table-header>
                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDateRecorded='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block xsl:use-attribute-sets="table-column-header ">
                                                            <xsl:value-of select="exsl:node-set($labels)/dateRecorded"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDescription='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block xsl:use-attribute-sets="table-column-header ">
                                                            <xsl:value-of select="exsl:node-set($labels)/description"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showTerm='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header">
                                                            <xsl:value-of select="exsl:node-set($labels)/term"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showCharge='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header numeric-align">
                                                            <xsl:value-of select="exsl:node-set($labels)/gridCharge"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showPayment='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header numeric-align">
                                                            <xsl:value-of select="exsl:node-set($labels)/gridPayment"/>
                                                        </fo:block>

                                                    </fo:table-cell>
                                                </xsl:if>
                                            </fo:table-row>
                                        </fo:table-header>
                                        <fo:table-body>
                                            <xsl:for-each select="studentAccountInformation/transactions/result">
                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                    <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDateRecorded='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="dateRecorder"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDescription='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="description"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showTerm='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="termDescription"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showCharge='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="chargeAmountDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showPayment='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="paymentAmountDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                </fo:table-row>
                                            </xsl:for-each>
                                            <!--Total-->
                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDateRecorded='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRow='true'">
                                                            <fo:block xsl:use-attribute-sets="table-row-header">
                                                                <xsl:value-of select="exsl:node-set($labels)/totalRow"/>
                                                            </fo:block>
                                                        </xsl:if>
                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRow='false'">
                                                            <fo:block xsl:use-attribute-sets="table-row-header">
                                                            </fo:block>
                                                        </xsl:if>
                                                    </fo:table-cell>
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <fo:block xsl:use-attribute-sets="table-row-header">
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showDescription='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <fo:block xsl:use-attribute-sets="table-row-header">
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showTerm='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <fo:block xsl:use-attribute-sets="table-row-header">
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showCharge='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                        <fo:block xsl:use-attribute-sets="margin">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/transactions/totals/totalchargeAmountDisplay"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/transactionGridConfig/showPayment='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                        <fo:block xsl:use-attribute-sets="margin">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/transactions/totals/totalpaymentAmountDisplay"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                </fo:block-container>
                            </xsl:if>
                        </xsl:if>
                        <xsl:if test="studentAccountInformation/displayTransactions='false'">
                            <fo:block-container xsl:use-attribute-sets="pageTitle">
                                <fo:block text-align='left'>
                                    <xsl:value-of select="exsl:node-set($labels)/transactionTitle"/>
                                </fo:block>
                            </fo:block-container>
                            <fo:block-container xsl:use-attribute-sets="pageInfo">
                                <fo:block text-align='left' xsl:use-attribute-sets="pageInfoText">
                                    <fo:external-graphic
                                            xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                    <fo:bidi-override
                                            unicode-bidi="embed"
                                            direction="ltr">
                                        <xsl:value-of
                                                select="studentAccountInformation/transactions/infoMsg"/>
                                    </fo:bidi-override>
                                </fo:block>
                            </fo:block-container>
                        </xsl:if>
                        <xsl:if test="studentAccountInformation/displayDeposits='true'">
                            <fo:block-container xsl:use-attribute-sets="pageTitle">
                                <fo:block text-align='left'>
                                    <xsl:value-of select="exsl:node-set($labels)/depositTitle"/>
                                </fo:block>
                            </fo:block-container>
                            <fo:block-container xsl:use-attribute-sets="pageInfo">
                                <fo:block text-align='left' xsl:use-attribute-sets="pageInfoText">
                                    <fo:external-graphic
                                            xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160; &#160;
                                    <fo:bidi-override
                                            unicode-bidi="embed"
                                            direction="ltr">
                                        <xsl:value-of
                                                select="studentAccountInformation/deposits/infoMsg"/>
                                    </fo:bidi-override>
                                </fo:block>
                            </fo:block-container>
                            <xsl:if test="studentAccountInformation/hasData='false'">
                                <fo:block-container xsl:use-attribute-sets="pageInfo">

                                    <fo:block
                                            xsl:use-attribute-sets="pageInfoText">
                                        &#160;&#160;<fo:external-graphic
                                            xsl:use-attribute-sets="accountSummary-info-circle-icon"/>&#160;
                                        &#160;<xsl:value-of
                                            select="studentAccountInformation/noRecordsInfoText"/>
                                    </fo:block>

                                </fo:block-container>
                            </xsl:if>

                            <xsl:if test="studentAccountInformation/deposits/showGrid='true'">
                                <fo:block xsl:use-attribute-sets="line-break">&#160;
                                </fo:block>
                                <fo:block-container>
                                    <fo:table xsl:use-attribute-sets="table">
                                        <fo:table-header>
                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDateRecorded='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block xsl:use-attribute-sets="table-column-header ">
                                                            <xsl:value-of select="exsl:node-set($labels)/dateRecorded"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDescription='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block xsl:use-attribute-sets="table-column-header ">
                                                            <xsl:value-of select="exsl:node-set($labels)/description"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showTerm='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header">
                                                            <xsl:value-of select="exsl:node-set($labels)/term"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showOriginalAmount='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header numeric-align">
                                                            <xsl:value-of select="exsl:node-set($labels)/origAmount"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showBalance='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                        <fo:block
                                                                xsl:use-attribute-sets="table-column-header numeric-align">
                                                            <xsl:value-of select="exsl:node-set($labels)/balance"/>
                                                        </fo:block>

                                                    </fo:table-cell>
                                                </xsl:if>
                                            </fo:table-row>
                                        </fo:table-header>
                                        <fo:table-body>
                                            <xsl:for-each select="studentAccountInformation/deposits/result">
                                                <fo:table-row xsl:use-attribute-sets="tableRow">
                                                    <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDateRecorded='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="dateRecorder"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDescription='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="description"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showTerm='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="termDescription"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showOriginalAmount='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="amountDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                    <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showBalance='true'">
                                                        <fo:table-cell
                                                                xsl:use-attribute-sets="accountSummaryOverview-data numeric-align tableRow">
                                                            <fo:block xsl:use-attribute-sets="margin">
                                                                <xsl:value-of
                                                                        select="balanceDisplay"/>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:if>
                                                </fo:table-row>
                                            </xsl:for-each>
                                            <!--Total-->
                                            <fo:table-row xsl:use-attribute-sets="tableRow">
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDateRecorded='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRow='true'">
                                                            <fo:block xsl:use-attribute-sets="table-row-header">
                                                                <xsl:value-of select="exsl:node-set($labels)/totalRow"/>
                                                            </fo:block>
                                                        </xsl:if>
                                                        <xsl:if test="exsl:node-set($config)/custom/showTotalRow='false'">
                                                            <fo:block xsl:use-attribute-sets="table-row-header">
                                                            </fo:block>
                                                        </xsl:if>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showDescription='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <fo:block xsl:use-attribute-sets="table-row-header">
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showTerm='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data totalRow">
                                                        <fo:block xsl:use-attribute-sets="table-row-header">
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showOriginalAmount='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                        <fo:block xsl:use-attribute-sets="margin">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/deposits/totals/totalamountDisplay"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                                <xsl:if test="exsl:node-set($config)/custom/depositGridConfig/showBalance='true'">
                                                    <fo:table-cell
                                                            xsl:use-attribute-sets="accountSummaryOverview-data numeric-align totalRow">
                                                        <fo:block xsl:use-attribute-sets="margin">
                                                            <xsl:value-of
                                                                    select="studentAccountInformation/deposits/totals/totalbalanceDisplay"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:if>
                                            </fo:table-row>
                                        </fo:table-body>
                                    </fo:table>
                                </fo:block-container>
                            </xsl:if>
                        </xsl:if>
                    </fo:block-container>
                    <fo:block-container xsl:use-attribute-sets="container disclaimer">
                        <fo:block xsl:use-attribute-sets="line-break">&#160;
                        </fo:block>
                        <xsl:if test="exsl:node-set($config)/custom/displayDisclaimerInfoOnPrintPreview = 'true'">
                            <fo:block xsl:use-attribute-sets="disclaimerHeader">
                                <xsl:value-of select="exsl:node-set($labels)/disclaimer"/>
                            </fo:block>
                            <fo:block xsl:use-attribute-sets="disclaimerMessage">
                                <xsl:value-of select="studentAccountInformation/disclaimerMessage"/>
                            </fo:block>
                        </xsl:if>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
