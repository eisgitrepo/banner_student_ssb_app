<?xml version="1.0" encoding="UTF-8" ?>
<!-- Copyright 2019 Ellucian Company L.P. and its affiliates. -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl">

    <xsl:attribute-set name="columnMargin">
        <xsl:attribute name="margin-left">16pt</xsl:attribute>
        <xsl:attribute name="margin-top">4pt</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>
