/*******************************************************************************
 Copyright 2016-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package net.hedtech.banner.studentssb.system
import grails.util.Holders
import groovy.sql.Sql
import net.hedtech.banner.general.person.PersonUtility
import net.hedtech.banner.studentssb.utils.ConsolidationSessionManager
import net.hedtech.banner.testing.BaseIntegrationTestCase
import org.junit.After
import org.junit.Before
import org.junit.Test

import java.sql.SQLException

import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration

@Integration
@Rollback
class WebTailorServiceIntegrationTests extends BaseIntegrationTestCase {

    def webTailorService
    def dataSource

    @Before
    public void setUp() {
        formContext = ['GUAGMNU']
        super.setUp()
        Holders?.config.ssbEnabled = true;
    }

    @After
    public void tearDown() {
        super.tearDown();
    }


    @Test
    void testGetTwgParamDefault() {
        String url
        url = webTailorService.getTwgParam (ConsolidationSessionManager.LANDING_PAGE)
        assertNotNull url
        assertEquals "studentCommonDashboard", url
    }

    @Test
    void testGetTwgParamCustom() {
        createTwgParamEntry()
        String url
        url = webTailorService.getTwgParam (ConsolidationSessionManager.LANDING_PAGE)
        assertNotNull url
        assertEquals "http://abc:8080/StudentFacultyGradeEntry/ssb/gradeEntry", url
    }

    @Test
    void testGetTwgMenuStudent()
    {
        def map
        def pidm = PersonUtility.getPerson("RDANIELS").pidm
        map = webTailorService.getTwgMenu(pidm,"http://abc:8080/StudentSelfService/ssb/studentAttendanceTracking", "studentAttendanceTracking")
        assertNotNull map.size() > 0

    }


    @Test
    void testGetTwgMenuFaculty()
    {
        def map
        def pidm = PersonUtility.getPerson("CBUNTE3").pidm
        map = webTailorService.getTwgMenu(pidm,"http://abc:8080/StudentSelfService/ssb/classListApp/classListPage", "Class List")
        assertNotNull map.size() > 0

    }


    private createTwgParamEntry(){
        def sql
        String twgParam = "UPDATE TWGBPARM SET TWGBPARM_PARAM_VALUE='http://abc:8080/StudentFacultyGradeEntry/ssb/gradeEntry' WHERE TWGBPARM_PARAM_NAME='STU_SS_LAND_PAGE'"
        try {
            sql = new Sql(sessionFactory.getCurrentSession().connection())
            sql.executeUpdate(twgParam)
        }
        catch (SQLException e) {
            throw e
        }
        finally {
           // sql?.close()
        }
    }

    private createTwgMenuEntryStudent() {
        def sql
        String twgMenu = "UPADTE TWGRMENU SET TWGRMENU_URL='http://abc:8080/StudentSelfService/ssb/studentAttendanceTracking' \n" +
                "WHERE TWGRMENU_NAME = 'bmenu.P_StuMainMnu' \n" +
                "AND (twgrmenu_url_text = 'Student Services: Attendance'\n" +
                "OR  twgrmenu_url like 'Track Attendance')"
        try {

            sql.executeUpdate(twgMenu)
        }
        catch (SQLException e) {
            throw e
        }
        finally {
            //sql?.close()

        }
    }

    private createTwgMenuEntryFaculty() {
        def sql
        String twgMenu = "UPADTE TWGRMENU SET TWGRMENU_URL='http://abc:8080/StudentSelfService/ssb/classListApp/classListPage' \n" +
                "WHERE TWGRMENU_NAME = 'bmenu.P_MainMnu' \n" +
                "AND (twgrmenu_url_text = 'Class List'\n" +
                "OR  twgrmenu_url like 'Class List')"
        try {

            sql.executeUpdate(twgMenu)
        }
        catch (SQLException e) {
            throw e
        }
        finally {
            //sql?.close()

        }
    }

}
