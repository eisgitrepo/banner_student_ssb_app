/** Copyright 2019 Ellucian Company L.P. and its affiliates. **/
package net.hedtech.banner.studentssb.system

import net.hedtech.banner.general.utility.PreferredNameService
import net.hedtech.banner.security.BannerGrantedAuthorityService
import net.hedtech.banner.testing.BaseIntegrationTestCase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder

import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration

@Integration
@Rollback
class StudentCommonDashboardControllerIntegrationTests extends BaseIntegrationTestCase {

    def preferredNameService
    def webTailorService
    def springSecurityService

    @Before
    public void setUp() {
        formContext = ['SELFSERVICE']
        controller = new StudentCommonDashboardController()
        controller.webTailorService = webTailorService
        controller.springSecurityService = springSecurityService
        controller.preferredNameService = preferredNameService
        super.setUp()
    }

    @After
    public void tearDown() {
        super.tearDown()
        logout()
    }

    @Test
    void testUserNotLoggedIn() {
        def result = controller.dashboard()
        assertEquals(401, controller.response.status)
        assertNull result
    }

    @Test
    void testStudentLogin() {
        String studentId = 'HOSH00018'
        logInUser(studentId, '111111')
        controller.dashboard()
        def response = controller.response
        assertEquals(200, response.status)
        /*assertNotNull( result )
        assertEquals( studentId, result.model.bannerId)
        assertEquals( getPreferredName(), result.model.preferredName)
        assertEquals( "studentCommonDashboard", result.view)*/
    }

    @Test
    void testFacultyLogin() {
        String facultyId = 'ARTHINC'
        logInUser(facultyId, '111111')
        controller.dashboard()
        def response = controller.response
        assertEquals(200, response.status)
        /*assertNotNull( result )
        assertEquals( facultyId, result.model.bannerId)
        assertEquals( getPreferredName(), result.model.preferredName)
        assertEquals( "studentCommonDashboard", result.view)*/
    }

    @Test
    void testCLAdminAndFacultyLogin() {
        String facultyId = 'CLADMIN02'
        logInUser(facultyId, '111111')
        controller.dashboard()
        def response = controller.response
        assertEquals(200, response.status)
        /*assertNotNull( result )
        assertEquals( facultyId, result.model.bannerId)
        assertEquals( getPreferredName(), result.model.preferredName)
        assertEquals( "studentCommonDashboard", result.view)*/
    }

    @Test
    void testCLAdminLogin() {
        String adminId = 'CLADMIN01'
        logInUser(adminId, '111111')
        def result = controller.dashboard()
        def response = controller.response
        assertEquals(200, response.status)
        assertEquals true, result
    }

    private String getPreferredName() {

        String pageName = 'Landing Page';
        String productName = 'Student';
        String appName = 'Student Self-Service';
        Map<String, String> params = new HashMap<String, String>();

        params.put("pidm", BannerGrantedAuthorityService.getPidm());
        params.put("productname", productName);
        params.put("appname", appName);
        params.put("pagename", pageName);
        return preferredNameService.getPreferredName(params);
    }

    private void logInUser(bannerId, password) {
        def auth = selfServiceBannerAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(bannerId, password))
        SecurityContextHolder.getContext().setAuthentication(auth)
    }

    protected void logout() {
        SecurityContextHolder.getContext().setAuthentication( null )
    }
}
