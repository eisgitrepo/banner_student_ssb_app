/*******************************************************************************
 Copyright 2018-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package net.hedtech.banner.studentssb.system

import net.hedtech.banner.testing.BaseIntegrationTestCase
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration

@Integration
@Rollback
class LandingPagePictureControllerIntegrationTests extends BaseIntegrationTestCase {

    def selfServiceBannerAuthenticationProvider

    @Before
    public void setUp() {
        formContext = ['SELFSERVICE']
        controller = new LandingPagePictureController()
        super.setUp()
        //Holders?.config.ssbEnabled = true;
    }

    @After
    public void tearDown() {
        super.tearDown();
        logout()
    }


    @Test
    void testUserNotLoggedIn() {
        controller.params.bannerId = 'ARTHINC'
        assertFalse controller.hasAccess()
        assertEquals(200, controller.response.status)
    }



    @Test
    void testStudentAccess() {
        logInUser('HOSH00018', '111111')

        assertTrue controller.hasAccess()
        assertEquals(200, controller.response.status)
    }

    @Test
    void testStudentAccess_Invalid_BannerId() {
        logInUser('HOSH00018', '111111')

        controller.params.bannerId = 'ARTHINC'
        assertFalse controller.hasAccess()
        assertEquals(200, controller.response.status)
    }

    @Test
    void testStudentAccess_Invalid_Null_BannerId() {
        logInUser('HOSH00018', '111111')

        controller.params.bannerId = null
        assertFalse controller.hasAccess()
        assertEquals(200, controller.response.status)
    }


    @Test
    void testFacultyAccess() {
        logInUser('ARTHINC', '111111')

        assertTrue controller.hasAccess()
        assertEquals(200, controller.response.status)
    }

    @Test
    void testFacultyAccess_Invalid_BannerId() {
        logInUser('ARTHINC', '111111')

        controller.params.bannerId = 'HOSH00018'
        assertFalse controller.hasAccess()
        assertEquals(200, controller.response.status)
    }

    @Test
    void testFacultyAccess_Invalid_Null_BannerId() {
        logInUser('ARTHINC', '111111')

        controller.params.bannerId = null
        assertFalse controller.hasAccess()
        assertEquals(200, controller.response.status)
    }



    private void logInUser(bannerId, password) {
        def auth = selfServiceBannerAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(bannerId, password))
        SecurityContextHolder.getContext().setAuthentication(auth)
        controller.params.bannerId = bannerId
    }

    protected void logout() {
        SecurityContextHolder.getContext().setAuthentication( null )
    }
}
